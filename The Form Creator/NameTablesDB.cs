﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Form_Creator
{
    class NameTablesDB
    {
        public const string Attributes = "Attributes";
        public const string Form = "Form";
        public const string ValuesAttrToForm = "ValuesAttrToForm";
        public const string AttrToTheForm = "AttributeToTheForm";
        public const string Functions = "AllFunctions";
        public const string Comments = "AllComments";
        public const string AttrFunctions = "AttributeFunctionsNEW";
        public const string AttrComments = "AttributeCommentsNEW";
        public const string Style = "StyleAttr";
        public const string Import = "Import";
        public const string ImportAttr = "ImportAttr";
        public const string ImportValues = "ImportValues";
        public const string ImportRows = "ImportRows";
    }
}
