﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace The_Form_Creator
{
    class DbBackup
    {
        public string backupPath;
        public string currentPath;
        public string dbPath;
        public string[] fileEntries;

        public DbBackup()
        {
            currentPath = Directory.GetCurrentDirectory();
            backupPath = currentPath + "\\Backup";
            dbPath = currentPath + "\\DB.db";
            fileEntries = Directory.GetFiles(backupPath);
        }

        public void Execute()
        {
            Dictionary<int, string> fileNames = new Dictionary<int, string>();

            for (int i = 0; i < fileEntries.Length; i++)
                fileNames.Add(i, fileEntries[i]);

            string[] separator = new string[] { "BackupDB_", ".db" };

            Dictionary<int, string[]> names = new Dictionary<int, string[]>();


            for (int i = 0; i < fileNames.Count; i++)
            {
                string item = fileNames[i];
                if (item.IndexOf("BackupDB_") > -1)
                {
                    names.Add(i, item.Split(separator, StringSplitOptions.None));

                }
            }

            Dictionary<int, int> numbers = new Dictionary<int, int>();

            foreach (var item in names)
            {
                int number;
                if (Int32.TryParse(item.Value[1], out number))
                    numbers.Add(item.Key, number);
            }

            int keyToAdd = (numbers.Count > 0) ? 1 + numbers.Values.Max() : 1;

            if (numbers.Count > 20)
            {

                int keyToDel = numbers.FirstOrDefault(x => x.Value == numbers.Values.Min()).Key;
                File.Delete(fileEntries[keyToDel]);
            }

            string destFile = System.IO.Path.Combine(Directory.GetCurrentDirectory() + "\\Backup\\", "BackupDB_" + keyToAdd + ".db");
            File.Copy(dbPath, destFile, true);
        }


    }
}


