﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace The_Form_Creator
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
                new FillerBox().FillListBoxWithForms(listBox1, conDB);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form2(), this);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            button2.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItems.Count == 0)
                return;

            DialogResult result = MessageBox.Show("Usunięcie spowoduje, że nie będzie można przywrócić tej formatki z zapisanymi ustawieniami. Możesz zmienić nazwę formatki zamiast ją usuwać nieodwracalnie. \n\nCzy na pewno chcesz usunąć tę pozycję?", "Uwaga!",
            MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
                using (SQLiteTransaction trans = conDB.BeginTransaction())
                    try
                    {
                        SQLiteEasierCommands sqliteCmd = new SQLiteEasierCommands(conDB);
                        int idFormToDelete = Convert.ToInt32(listBox1.SelectedValue);
                        DeleterValuesDB deleter = new DeleterValuesDB(sqliteCmd);

                        deleter.DeleteForm(idFormToDelete);


                        new FillerBox().FillListBoxWithForms(listBox1, conDB);
                        button2.Enabled = false;
                        trans.Commit();
                        MessageBox.Show("Formatka usunięta pomyślnie");
                    }
                    catch (Exception exc)
                    {
                        trans.Rollback();
                        string textError = "Błąd podczas usuwania formatki\n";

                        LogsManager.saveErrorTxt(textError + "\n" + exc.Message);
                        MessageBox.Show(textError, "Błąd");
                    }


            }


        }
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            Application.Exit();
        }

        private void listBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == (char)Keys.Delete)
                button2_Click(sender, e);
        }
    }
}
