﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;


namespace The_Form_Creator
{
    public partial class Form12 : Form
    {
        public Form12()
        {
            InitializeComponent();
        }

        private void Form12_Load(object sender, EventArgs e)
        {
            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
            {
                new FillerBox().FillListBoxWithAttrs(listBox1, conDB);
            }
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            Application.Exit();
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
                panel1.BackColor = colorDialog1.Color;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form6(), this);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int idAttr = Convert.ToInt32(listBox1.SelectedValue);
            string color = ColorTranslator.ToHtml(panel1.BackColor);

            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
            using (SQLiteTransaction trans = conDB.BeginTransaction())
            {
                try
                {
                    SQLiteEasierCommands sqliteCmd = new SQLiteEasierCommands(conDB);
                    InserterValuesDB inserter = new InserterValuesDB(sqliteCmd);

                    inserter.InsertStyle(idAttr, color);

                    trans.Commit();
                    MessageBox.Show("Kolor dodany pomyślnie");
                }
                catch (Exception exc)
                {
                    trans.Rollback();
                    string textError = "Błąd podczas przypisywania koloru. Prawdopodobnie atrybut został usunięty.";
                    LogsManager.saveErrorTxt(textError + " KOLOR ATTR" + exc.Message);
                    MessageBox.Show(textError);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int idAttr = Convert.ToInt32(listBox1.SelectedValue);

            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
            using (SQLiteTransaction trans = conDB.BeginTransaction())
            {
                try
                {
                    SQLiteEasierCommands sqliteCmd = new SQLiteEasierCommands(conDB);
                    DeleterValuesDB deleter = new DeleterValuesDB(sqliteCmd);

                    deleter.DeleteStyle(idAttr);

                    panel1.BackColor = DefaultBackColor;
                    MessageBox.Show("Kolor usunięty pomyślnie");
                    trans.Commit();
                }
                catch (Exception exc)
                {
                    trans.Rollback();
                    string textError = "Błąd podczas usuwania koloru. Prawdopodobnie atrybut został usunięty.";
                    LogsManager.saveErrorTxt(textError + " KOLOR ATTR" + exc.Message);
                    MessageBox.Show(textError);
                }
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            button1.Enabled = true;
            button2.Enabled = true;

            int idAttr = Convert.ToInt32(listBox1.SelectedValue);

            SQLiteEasierCommands sqliteCmd = new SQLiteEasierCommands(new SQLiteConnector().getSQLiteConnection());
            GetterValuesAttr getterAttr = new GetterValuesAttr(sqliteCmd);

            string color = getterAttr.GetStyle(idAttr);

            if (color.Length > 0)
                panel1.BackColor = System.Drawing.ColorTranslator.FromHtml(color);
            else
                panel1.BackColor = DefaultBackColor;

        }
    }
}
