﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Form_Creator
{
    public partial class Form11 : Form
    {
        public Form11()
        {
            InitializeComponent();
        }

        private void Form11_Load(object sender, EventArgs e)
        {
            SQLiteEasierCommands sqliteCmd = new SQLiteEasierCommands(new SQLiteConnector().getSQLiteConnection());
            GetterValuesForm getterValuesForm = new GetterValuesForm(sqliteCmd);

            List<string> formFunctions = getterValuesForm.GetFunctions(Form9.selectedFormName);
            List<string> formComments = getterValuesForm.GetComments(Form9.selectedFormName);
            List<string> formAttributes = getterValuesForm.GetAllAttributesAssignedTo(Form9.selectedFormName);
            FillerBox fillerBox = new FillerBox();

            fillerBox.FillTextBoxByList(textBox2, formFunctions);
            fillerBox.FillTextBoxByList(textBox1, formComments);
            fillerBox.FillTextBoxByList(textBox3, formAttributes);

            label1.Text = Form9.selectedFormName;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
