﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Form_Creator
{
    class DeleterValuesDB
    {
        private SQLiteEasierCommands sqliteCmd;

        public DeleterValuesDB(SQLiteEasierCommands sqliteCmd)
        {
            this.sqliteCmd = sqliteCmd;
        }


        public bool DeleteStyle(int idAttr)
        {
            string[] paramsArray = new string[] { idAttr.ToString() };

            return ExecuteToInt(GetQueryWithOneValue(NameTablesDB.Style, "Id_Attribute"), paramsArray);
        }

        public bool DeleteValuesAttrToForm(int idValuesAttrToForm)
        {
            DeleteAttributeFunctionsNEWByIdValuesAttrToForm(idValuesAttrToForm);
            DeleteAttributeCommentsNEWByIdValuesAttrToForm(idValuesAttrToForm);

            string[] paramsArray = new string[] { idValuesAttrToForm.ToString() };
            return ExecuteToInt(GetQueryWithOneValue(NameTablesDB.ValuesAttrToForm, "Id"), paramsArray);
        }

        private bool DeleteValuesAttrToFormByIdForm(int idForm)
        {
            string[] paramsArray = new string[] { idForm.ToString() };

            return ExecuteToInt(GetQueryWithOneValue(NameTablesDB.ValuesAttrToForm, "Id_Form"), paramsArray);
        }

        private bool DeleteValuesAttrToFormByIdAttr(int idAttr)
        {
            string[] paramsArray = new string[] { idAttr.ToString() };

            return ExecuteToInt(GetQueryWithOneValue(NameTablesDB.ValuesAttrToForm, "Id_Attribute"), paramsArray);
        }

        public bool DeleteComment(int idAllComments)
        {
            DeleteAttributeCommentsNEWByIdFunction(idAllComments);

            string[] paramsArray = new string[] { idAllComments.ToString() };
            return ExecuteToInt(GetQueryWithOneValue(NameTablesDB.Comments, "Id"), paramsArray);
        }

        public bool DeleteComment(string ExcelComment)
        {
            DeleteAttributeCommentsNEW(ExcelComment);
            string[] paramsArray = new string[] { ExcelComment };
            return ExecuteToInt(GetQueryWithOneValue(NameTablesDB.Comments, "ExcelComment"), paramsArray);
        }

        public bool DeleteAttributeCommentsNEWByIdFunction(int idAllComments)
        {
            string[] paramsArray = new string[] { idAllComments.ToString() };

            return ExecuteToInt(GetQueryWithOneValue(NameTablesDB.Comments, "Id_AllComments"), paramsArray);
        }

        public bool DeleteAttributeCommentsNEW(string ExcelComment)
        {
            string[] paramsArray = new string[] { ExcelComment };
            ExecuteToInt(GetQueryWithOneValueAndOneReferenceConditionTable2(NameTablesDB.AttrComments, NameTablesDB.Comments, "Id_AllFunctions", "Id", "ExcelComments"), paramsArray);

            return ExecuteToInt(GetQueryWithOneValue(NameTablesDB.AttrComments, "ExcelComment"), paramsArray);
        }

        public bool DeleteAttributeCommentsNEWByIdValuesAttrToForm(int idValuesAttrToForm)
        {
            string[] paramsArray = new string[] { idValuesAttrToForm.ToString() };

            return ExecuteToInt(GetQueryWithOneValue(NameTablesDB.AttrComments, "Id_ValuesAttrToForm"), paramsArray);
        }

        public bool DeleteFunction(int idAllFunctions)
        {
            DeleteAttributeFunctionsNEWByIdFunction(idAllFunctions);

            string[] paramsArray = new string[] { idAllFunctions.ToString() };
            return ExecuteToInt(GetQueryWithOneValue(NameTablesDB.Functions, "Id"), paramsArray);
        }

        public bool DeleteFunction(string ExcelFunction)
        {
            DeleteAttributeFunctionsNEW(ExcelFunction);
            string[] paramsArray = new string[] { ExcelFunction };
            return ExecuteToInt(GetQueryWithOneValue(NameTablesDB.Functions, "ExcelFunction"), paramsArray);
        }

        public bool DeleteAttributeFunctionsNEWByIdFunction(int idAllFunctions)
        {
            string[] paramsArray = new string[] { idAllFunctions.ToString() };

            return ExecuteToInt(GetQueryWithOneValue(NameTablesDB.AttrFunctions, "Id_AllFunctions"), paramsArray);
        }

        public bool DeleteAttributeFunctionsNEW(string ExcelFunction)
        {
            string[] paramsArray = new string[] { ExcelFunction };
            ExecuteToInt(GetQueryWithOneValueAndOneReferenceConditionTable2(NameTablesDB.AttrFunctions, NameTablesDB.Functions, "Id_AllFunctions", "Id", "ExcelFunction"), paramsArray);

            return ExecuteToInt(GetQueryWithOneValue(NameTablesDB.AttrFunctions, "ExcelFunction"), paramsArray);
        }

        public bool DeleteAttributeFunctionsNEWByIdValuesAttrToForm(int idValuesAttrToForm)
        {
            string[] paramsArray = new string[] { idValuesAttrToForm.ToString() };

            return ExecuteToInt(GetQueryWithOneValue(NameTablesDB.AttrFunctions, "Id_ValuesAttrToForm"), paramsArray);
        }

        public void DeleteAttributeFunctionsNEWByIdForm(int idForm)
        {
            string[] paramsArray = new string[] { idForm.ToString() };
            ExecuteToInt(GetQueryWithOneValueAndOneReferenceConditionTable2(NameTablesDB.AttrFunctions, NameTablesDB.ValuesAttrToForm, "Id_ValuesAttrToForm", "Id", "Id_Form"), paramsArray);
        }

        public bool DeleteAttributeFunctionsNEWByIdAttr(int idAttr) ///////TADY
        {
            string[] paramsArray = new string[] { idAttr.ToString() };
            return ExecuteToInt(GetQueryWithOneValueAndOneReferenceConditionTable2(NameTablesDB.AttrFunctions, NameTablesDB.ValuesAttrToForm, "Id_ValuesAttrToForm", "Id", "Id_Attribute"), paramsArray);
        }

        public void DeleteAttributeCommentsNEWByIdForm(int idForm)
        {
            string[] paramsArray = new string[] { idForm.ToString() };
            ExecuteToInt(GetQueryWithOneValueAndOneReferenceConditionTable2(NameTablesDB.AttrComments, NameTablesDB.ValuesAttrToForm, "Id_ValuesAttrToForm", "Id", "Id_Form"), paramsArray);
        }

        public bool DeleteAttributeCommentsNEWByIdAttr(int idAttr)
        {
            string[] paramsArray = new string[] { idAttr.ToString() };
            return ExecuteToInt(GetQueryWithOneValueAndOneReferenceConditionTable2(NameTablesDB.AttrComments, NameTablesDB.ValuesAttrToForm, "Id_ValuesAttrToForm", "Id", "Id_Attribute"), paramsArray);
        }

        public void DeleteForm(int idForm)
        {
            DeleteAttributeFunctionsNEWByIdForm(idForm);
            DeleteAttributeCommentsNEWByIdForm(idForm);
            DeleteValuesAttrToFormByIdForm(idForm);
            DeleteAttributeToTheFormByForm(idForm);

            string[] paramsArray = new string[] { idForm.ToString() };

            ExecuteToInt(GetQueryWithOneValue(NameTablesDB.Form, "Id"), paramsArray);
        }

        public bool DeleteAttr(int idAttr)
        {
            DeleteAttributeFunctionsNEWByIdAttr(idAttr);
            DeleteAttributeCommentsNEWByIdAttr(idAttr);
            DeleteValuesAttrToFormByIdAttr(idAttr);
            DeleteAttributeToTheFormByAttr(idAttr);
            DeleteStyle(idAttr);

            string[] paramsArray = new string[] { idAttr.ToString() };

            return ExecuteToInt(GetQueryWithOneValue(NameTablesDB.Attributes, "Id"), paramsArray);
        }

        public void DeleteAttributeToTheFormByForm(int idForm)
        {
            string[] paramsArray = new string[] { idForm.ToString() };

            ExecuteToInt(GetQueryWithOneValue(NameTablesDB.AttrToTheForm, "Id_Form"), paramsArray);
        }

        public bool DeleteAttributeToTheFormByAttr(int idAttr)
        {
            string[] paramsArray = new string[] { idAttr.ToString() };

            return ExecuteToInt(GetQueryWithOneValue(NameTablesDB.AttrToTheForm, "Id_Attribute"), paramsArray);
        }

        public bool DeleteAttributeToTheForm(int idAttr, int idForm)
        {
            string[] paramsArray = new string[] { idAttr.ToString(), idForm.ToString() };

            return ExecuteToInt(GetQueryWithTwoValues(NameTablesDB.AttrToTheForm, "Id_Attribute", "Id_Form"), paramsArray);

        }

        private bool ExecuteToInt(string query, string[] paramsArray)
        {
            return sqliteCmd.ExecuteNonQueryWithParams(query, paramsArray);
        }

        private string GetQueryWithTwoValues(string nameTable, string column1, string column2)
        {
            return "DELETE FROM @TABLE WHERE @column1 = @param1 AND @column2 = @param2".Replace("@TABLE", nameTable).Replace("@column1", column1).Replace("@column2", column2);
        }

        private string GetQueryWithOneValue(string nameTable, string column1)
        {
            return "DELETE FROM @TABLE WHERE @column1 = @param1".Replace("@TABLE", nameTable).Replace("@column1", column1);
        }


        private string GetQueryWithOneValueAndOneReferenceConditionTable2(string nameTable, string nameTable2, string column1, string column2, string column3)
        {
//            return @"DELETE FROM @TABLE INNER JOIN @TABLE2 ON @TABLE2.@column2 = @column1 WHERE @TABLE2.@column3 = @param1"
//.Replace("@TABLE2", nameTable2).Replace("@TABLE", nameTable).Replace("@column1", column1).Replace("@column2", column2).Replace("@column3", column3);

            return @"DELETE FROM @TABLE WHERE @column1 IN (SELECT @column2 FROM @TABLE2 WHERE @TABLE2.@column3 = @param1)".
                Replace("@TABLE2", nameTable2).Replace("@TABLE", nameTable).Replace("@column1", column1).Replace("@column2", column2).Replace("@column3", column3);


            //string[] paramsArray = new string[] { idAttr.ToString() };
            //return ExecuteToInt(GetQueryWithOneValueAndOneReferenceConditionTable2(NameTablesDB.AttrFunctions, NameTablesDB.ValuesAttrToForm, "Id_ValuesAttrToForm", "Id", "Id_Attribute"), paramsArray);
        }

    }
}
