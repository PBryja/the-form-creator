﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
namespace The_Form_Creator
{
    class SQLiteReaderToListConverter
    {
        public List<string> getListFromReader(SQLiteDataReader reader)
        {
            if (reader.HasRows)
            {
                List<string> list = new List<string>();
                while (reader.Read())
                {
                    list.Add(reader[0].ToString().Trim());
                }
                return list;
            }
            else
                return null;
        }

    }
}
