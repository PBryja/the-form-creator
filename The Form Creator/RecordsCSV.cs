﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Form_Creator
{
    class RecordsCSV
    {
        private string name;
        public string nameAttribute { get; set; }

        public List<string> fields = new List<string>();

        public RecordsCSV(string name)
        {
            this.name = name;
        }

        public override string ToString()
        {
            return name;
        }
    }
}
