﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Form_Creator
{
    public partial class Form15 : Form
    {
        private string textError;
        private List<RecordsCSV> records = new List<RecordsCSV>();
        private List<ComboBox> listComboBox = new List<ComboBox>();
        private List<Label> listLabel = new List<Label>();

        public Form15()
        {
            InitializeComponent();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            Application.Exit();
        }

        private void resetPanel()
        {
            foreach (var item in listLabel)
                panel2.Controls.Remove(item);
            foreach (var item in listComboBox)
                panel2.Controls.Remove(item);



        }

        private void label1_DragDrop(object sender, DragEventArgs e)
        {
            string[] fileList = (string[])e.Data.GetData(DataFormats.FileDrop, false);

            resetPanel();
            records = new List<RecordsCSV>();
            listComboBox = new List<ComboBox>();
            listLabel = new List<Label>();

            using (var fs = File.OpenRead(fileList[0]))
            using (var reader = new StreamReader(fs))
            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
            {
                try
                {
                    FillerBox filler = new FillerBox();

                    bool first = true;
                    int length = 0;
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(';');
                        int x = 25;
                        int y = 25;
                        if (first)
                        {
                            length = values.Length;

                            for (int i = 0; i < length; i++)
                            {
                                Label label = new Label();
                                label.Text = values[i];
                                panel2.Controls.Add(label);
                                label.Location = new System.Drawing.Point(x, y);
                                listLabel.Add(label);
                                label.Width = 200;
                                records.Add(new RecordsCSV(values[i]));

                                ComboBox comboBox = new ComboBox();
                                panel2.Controls.Add(comboBox);
                                comboBox.Location = new System.Drawing.Point(x + 200, y);
                                listComboBox.Add(comboBox);
                                comboBox.Width = 200;
                                comboBox.MouseWheel += new MouseEventHandler(comboBox_MouseWheel);

                                filler.FillComboBoxWithAttrs(comboBox, conDB, true);

                                foreach (var item in comboBox.Items)
                                {
                                    DataRowView drw = item as DataRowView;

                                    if (values[i].ToLower().Equals(drw[comboBox.DisplayMember].ToString().ToLower()))
                                    {
                                        comboBox.SelectedValue = drw[comboBox.ValueMember].ToString();
                                        break;
                                    }
                                }

                                y += 25;
                            }
                            first = false;
                        }
                        else
                            for (int i = 0; i < length; i++)
                                records[i].fields.Add(values[i]);
                    }
                }
                catch (Exception exc)
                {
                    string textError = "Błąd podczas wczytywania pliku";
                    LogsManager.saveErrorTxt(textError + " COPY FORM" + exc.Message);
                    MessageBox.Show(textError);
                }
            }
        }


        private void label1_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;

        }

        private void Form15_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        void comboBox_MouseWheel(object sender, MouseEventArgs e)
        {
            ((HandledMouseEventArgs)e).Handled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length < 2)
            {
                MessageBox.Show("Podpisz się! Minimum 2 znaki.");
                return;
            }
            defaultComboBox();

            for (int i = 0; i < listComboBox.Count; i++)
                for (int j = 0; j < listComboBox.Count; j++)
                    if (i != j)
                        if (listComboBox[i].SelectedValue.ToString().Equals(listComboBox[j].SelectedValue.ToString()) && !String.IsNullOrEmpty(listComboBox[i].SelectedValue.ToString()))
                        {
                            MessageBox.Show("Uwaga! Atrybuty nie mogą się powtarzać. Popraw dane.");
                            listComboBox[i].BackColor = Color.Red;
                            listComboBox[j].BackColor = Color.Red;
                            return;
                        }

            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
            using (SQLiteTransaction trans = conDB.BeginTransaction())
            {
                try
                {

                    SQLiteEasierCommands sqliteCmd = new SQLiteEasierCommands(conDB);
                    InserterValuesDB inserter = new InserterValuesDB(sqliteCmd);
                    GetterValuesAttr getterAttr = new GetterValuesAttr(sqliteCmd);

                    int idImport = inserter.InsertImport(textBox1.Text);


                    if (idImport == 0)
                    {
                        trans.Rollback();
                        textError = "Wystąpił nieznany błąd podczas importu!";
                        LogsManager.saveErrorTxt(textError + " IMPORT");
                        MessageBox.Show(textError);
                        return;
                    }

                    List<int> indexToDel = new List<int>();
                    List<RecordsCSV> recordsTemp = records.ToList<RecordsCSV>();
                    for (int i = 0; i < recordsTemp.Count; i++)
                    {
                        if (String.IsNullOrEmpty(listComboBox[i].SelectedValue.ToString()))
                            indexToDel.Add(i);
                        else
                            recordsTemp[i].nameAttribute = ((DataRowView)listComboBox[i].SelectedItem).Row[1].ToString();

                    }
                    for (int i = 0; i < indexToDel.Count; i++)
                    {
                        int index = indexToDel[i] - i;
                        recordsTemp.RemoveAt(index);
                    }

                    if (recordsTemp.Count < 1)
                    {
                        trans.Rollback();
                        MessageBox.Show("Brak danych do importu");
                        return;
                    }

                    for (int i = 0; i < recordsTemp[0].fields.Count; i++) // wiersze po kolei
                    {
                        int idRow = inserter.InsertImportRow(idImport);

                        if (idRow == 0)
                        {
                            trans.Rollback();
                            textError = "Wystąpił nieznany błąd podczas importu!";
                            LogsManager.saveErrorTxt(textError + " IMPORT");
                            MessageBox.Show(textError);
                            return;
                        }

                        for (int j = 0; j < recordsTemp.Count; j++) //wartosci kolumny 
                        {
                            var column = recordsTemp[j];
                            if (!String.IsNullOrEmpty(column.fields[i].Trim()))
                            {
                                if (inserter.InsertImportValue(column.nameAttribute, idRow, column.fields[i]) < 0)
                                {
                                    trans.Rollback();
                                    textError = "Błąd podczas przypisywania wartości";
                                    LogsManager.saveErrorTxt(textError + " COPY FORM");
                                    MessageBox.Show(textError);
                                    return;
                                }
                            }
                        }
                    }

                    trans.Commit();
                    MessageBox.Show("Import wykonany poprawnie");
                }
                catch (Exception exc)
                {
                    trans.Rollback();
                    textError = "Bląd podczas importu\n";

                    LogsManager.saveErrorTxt(textError + exc.Message);
                    MessageBox.Show(textError, "Błąd");
                }
            }
        }

        private void defaultComboBox()
        {
            foreach (var item in listComboBox)
            {
                item.BackColor = SystemColors.Window;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form14(), this);
        }


        //Microsoft.Office.Interop.Excel.Application oXL;
        //Microsoft.Office.Interop.Excel.Workbook oWB;
        //Microsoft.Office.Interop.Excel.Worksheet oSheet;
        //Microsoft.Office.Interop.Excel.Range oRng2;

        //oXL = new Microsoft.Office.Interop.Excel.Application();

        //var workBooks = oXL.Workbooks;

        //oWB = (Microsoft.Office.Interop.Excel.Workbook)workBooks.Add("");
        //oSheet = (Microsoft.Office.Interop.Excel.Worksheet)oWB.ActiveSheet;
        //for (int i = 0; i < records[0].fields.Count; i++)
        //{
        //    for (int j = 0; j < records.Count; j++)
        //    {
        //        var asd = records[j];
        //        if (i == 0)
        //            oSheet.Cells[i + 1, j + 1] = asd.ToString();

        //            oSheet.Cells[i + 2, j + 2] = asd.fields[i];


        //    }
        //}
        //oXL.Visible = true;



    }
}
