﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Form_Creator
{
    public partial class Form16 : Form
    {
        public Form16()
        {
            InitializeComponent();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            Application.Exit();
        }

        private List<string> GetHeaders(List<List<Dictionary<string, string>>> listHeadersAndValuesInRows)
        {
            List<string> headers = new List<string>();
            foreach (List<Dictionary<string, string>> rows in listHeadersAndValuesInRows)
            {
                foreach (Dictionary<string, string> values in rows)
                {
                    foreach (KeyValuePair<string, string> value in values)
                    {
                        if (!headers.Contains(value.Key))
                        {
                            headers.Add(value.Key);
                        }

                    }
                }
            }
            return headers;
        }

        private List<string> GetHeaders(GetterValuesForm getter)
        {
            List<string> headers = new List<string>();
            headers = getter.GetAllAttributesAssignedTo(((DataRowView)comboBox5.SelectedItem).Row[1].ToString());
            return headers;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
            {
                try
                {

                
                List<List<Dictionary<string, string>>> listHeadersAndValuesInRows = new List<List<Dictionary<string, string>>>();
                List<string> idRows = new List<string>();
                SQLiteEasierCommands sqliteCmd = new SQLiteEasierCommands(conDB);
                string basic = "SELECT DISTINCT ImportValues.Id_Row From"; //, Import, ImportRows, ImportAttr
                string from = " ImportValues ";
                string where = "";

                if (!(((DataRowView)comboBox2.SelectedItem).Row[0].ToString().Equals("Wybierz")))
                {
                    if (!where.Contains("WHERE"))
                        where += " WHERE ";
                    else
                        where += " AND ";
                    where += " Import.Person LIKE '" + ((DataRowView)comboBox2.SelectedItem).Row[0].ToString()+ "' AND Import.Id = ImportRows.Id_Import AND ImportValues.Id_Row = ImportRows.Id";
                    from += ", Import , ImportRows ";
                }

                if (!String.IsNullOrEmpty(comboBox3.SelectedValue.ToString()))
                {
                    if (!where.Contains("WHERE"))
                        where += " WHERE ";
                    else
                        where += " AND ";

                    where += " Id_ImportAttr = " + comboBox3.SelectedValue + " AND ImportValues.Value LIKE '" + textBox1.Text + "'";
                    from += ", ImportAttr ";

                }

                if (!String.IsNullOrEmpty(comboBox1.SelectedValue.ToString()))
                {

                    if (!where.Contains("WHERE"))
                        where += " WHERE ";
                    else
                        where += " AND ";

                    // where += " Import.Id = ImportRows.Id_Import AND ImportValues.Id_Row = ImportRows.Id AND Import.Id = " + comboBox1.SelectedValue;
                    if ((((DataRowView)comboBox2.SelectedItem).Row[0].ToString().Equals("Wybierz")))
                    {
                        from += ", Import , ImportRows ";
                        where += " Import.Id = ImportRows.Id_Import AND ImportValues.Id_Row = ImportRows.Id AND Import.Id = " + comboBox1.SelectedValue;
                    }
                    else
                        where +=" Import.Id = " + comboBox1.SelectedValue;


                }
                SQLiteDataReader reader = sqliteCmd.GetExecutedReader(basic+from+where);
                int count = 0;

                while (reader.Read())
                {
                    idRows.Add(reader[0].ToString());
                    count++;
                }
               
                reader.Close();

                List<string> headers = new List<string>();

                for (int i = 0; i < idRows.Count; i++)
                {
                    reader = sqliteCmd.GetExecutedReader("SELECT ImportAttr.Name, ImportValues.Value FROM ImportValues, ImportAttr WHERE ImportValues.Id_Row = " + idRows[i] + " AND ImportAttr.Id = ImportValues.Id_ImportAttr");
                    List<Dictionary<string, string>> rowWithValues = new List<Dictionary<string, string>>();
                    while (reader.Read())
                    {
                        if (!(headers.Contains(reader[0].ToString())))
                            headers.Add(reader[0].ToString());

                        Dictionary<string, string> headerAndValue = new Dictionary<string, string>();
                        headerAndValue.Add(reader[0].ToString(), reader[1].ToString());
                        rowWithValues.Add(headerAndValue);
                    }
                    listHeadersAndValuesInRows.Add(rowWithValues);
                    reader.Close();
                }

                List<List<string>> valuesToExport = new List<List<string>>();

                if (!(String.IsNullOrEmpty(comboBox5.SelectedValue.ToString())))
                    headers = GetHeaders(new GetterValuesForm(sqliteCmd));

                foreach (var item in headers)
                {
                    List<string> temp = new List<string>();
                    foreach (List<Dictionary<string, string>> rows in listHeadersAndValuesInRows)
                    {
                        string temp2 = string.Empty;
                        foreach (Dictionary<string, string> values in rows)
                        {
                            if (values.ContainsKey(item))
                            {
                                temp2 = values[item];
                                continue;
                            }
                        }
                        temp.Add(temp2);
                    }
                    valuesToExport.Add(temp);
                }

                Microsoft.Office.Interop.Excel.Application oXL;
                Microsoft.Office.Interop.Excel.Workbook oWB;
                Microsoft.Office.Interop.Excel.Worksheet oSheet;

                oXL = new Microsoft.Office.Interop.Excel.Application();

                var workBooks = oXL.Workbooks;

                oWB = (Microsoft.Office.Interop.Excel.Workbook)workBooks.Add("");
                oSheet = (Microsoft.Office.Interop.Excel.Worksheet)oWB.ActiveSheet;

                oSheet.Cells.NumberFormat = "@";
                for (int i = 0; i < headers.Count; i++)
                {
                    oSheet.Cells[1, i + 1] = headers[i];
                }

                for (int i = 0; i < valuesToExport.Count; i++)
                {
                    for (int j = 0; j < valuesToExport[i].Count; j++)
                    {
                        oSheet.Cells[j + 2, i + 1].NumberFormat = "@";
                        oSheet.Cells[j + 2, i + 1] = valuesToExport[i][j];
                    }
                }

                oXL.Visible = true;
                Marshal.ReleaseComObject(oWB);
                Marshal.ReleaseComObject(oXL);
                Marshal.ReleaseComObject(workBooks);
                }
                catch (Exception exc)
                {
                    string textError = "Błąd podczas eksportu.\n";

                    LogsManager.saveErrorTxt(textError + exc.Message);
                    MessageBox.Show(textError, "Błąd");
                }
            }
        }

        private void Form16_Load(object sender, EventArgs e)
        {
            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
            {
                SQLiteEasierCommands sqliteCmd = new SQLiteEasierCommands(conDB);

                new FillerBox().FillComboBoxWithForms(comboBox5, conDB, true);
                new FillerBox().FillComboBoxWithImportAttrs(comboBox3, conDB, true);
                new FillerBox().FillComboBoxWithPerson(comboBox2, conDB, true);
                new FillerBox().FillComboBoxWithDates(comboBox1, conDB, true);
            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
            {
                SQLiteDataAdapter adapter = new SQLiteDataAdapter("SELECT DISTINCT Value From ImportValues, ImportAttr Where ImportAttr.Name= '" + ((DataRowView)comboBox3.SelectedItem).Row[1].ToString() + "' AND ImportAttr.Id = ImportValues.Id_ImportAttr", conDB);
                DataTable dTable = new DataTable();
                //
                adapter.Fill(dTable);

                comboBox4.DisplayMember = "Value";

                comboBox4.DataSource = dTable;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form14(), this);
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox1.Text = ((DataRowView)comboBox4.SelectedItem).Row[0].ToString();
        }
    }
}
