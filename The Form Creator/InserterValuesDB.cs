﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Form_Creator
{
    class InserterValuesDB
    {
        private SQLiteEasierCommands sqliteCmd;

        public InserterValuesDB(SQLiteEasierCommands sqliteCmd)
        {
            this.sqliteCmd = sqliteCmd;

        }

        public int InsertStyle(int idAttr, string color)
        {
            string[] paramsArray = new string[] { idAttr.ToString(), color.Trim() };

            return Convert.ToInt32(sqliteCmd.ExecuteScalarWithParams(GetQueryAttrFunctionsOrCommentsOrStyles(NameTablesDB.Style, "Id_Attribute", "Color"), paramsArray));
        }

        public int InsertForm(string nameForm)
        {
            string[] paramsArray = new string[] { nameForm.Trim() };

            return Convert.ToInt32(sqliteCmd.ExecuteScalarWithParams(GetQueryWithOneValue(NameTablesDB.Form, "Name"), paramsArray));
        }

        public int InsertAttribute(string nameAttr, int position)
        {
            string[] paramsArray = new string[] {  nameAttr.Trim(),  position.ToString()};

            return Convert.ToInt32(sqliteCmd.ExecuteScalarWithParams(GetQueryWithTwoValues(NameTablesDB.Attributes, "Name", "Position"), paramsArray));
        }

        public int InsertFunction(string text)
        {
            string[] paramsArray = new string[] { text.Trim() };

            return Convert.ToInt32(sqliteCmd.ExecuteScalarWithParams(GetQueryWithOneValue(NameTablesDB.Functions, "ExcelFunction"), paramsArray));
        }

        public int InsertComment(string text)
        {
            string[] paramsArray = new string[] { text.Trim() };

            return Convert.ToInt32(sqliteCmd.ExecuteScalarWithParams(GetQueryWithOneValue(NameTablesDB.Comments, "ExcelComment"), paramsArray));
        }

        public int AssignAttrToForm(int idAttr, int idForm)
        {
            string[] paramsArray = new string[] { idAttr.ToString(), idForm.ToString() };

            return Convert.ToInt32(sqliteCmd.ExecuteScalarWithParams(GetQueryWithTwoValues(NameTablesDB.AttrToTheForm, "Id_Attribute", "Id_Form"), paramsArray));
        }

        public int AssignFunctionToAttrAndForm(int idAttr, int idForm, string excelFunction)
        {
            int idFunction = InsertFunction(excelFunction);
            int idValuesAttrToForm = AssignValuesAttrToForm(idAttr, idForm);
            return AssignFunctionToAttr(idValuesAttrToForm, idFunction);
        }

        public int AssignCommentToAttrAndForm(int idAttr, int idForm, string excelComment)
        {
            int idComment = InsertComment(excelComment);
            int idValuesAttrToForm = AssignValuesAttrToForm(idAttr, idForm);
            return AssignCommentToAttr(idValuesAttrToForm, idComment);
        }

        private int AssignFunctionToAttr(int idValuesAttrToForm, int idAllFunctions)
        {
            string[] paramsArray = new string[] { idValuesAttrToForm.ToString(), idAllFunctions.ToString() };

            return Convert.ToInt32(sqliteCmd.ExecuteScalarWithParams(GetQueryAttrFunctionsOrCommentsOrStyles(NameTablesDB.AttrFunctions, "Id_ValuesAttrToForm", "Id_AllFunctions"), paramsArray));
        }

        private int AssignCommentToAttr(int idValuesAttrToForm, int idAllComments)
        {
            string[] paramsArray = new string[] { idValuesAttrToForm.ToString(), idAllComments.ToString() };

            return Convert.ToInt32(sqliteCmd.ExecuteScalarWithParams(GetQueryAttrFunctionsOrCommentsOrStyles(NameTablesDB.AttrComments, "Id_ValuesAttrToForm", "id_AllComments"), paramsArray));
        }

        private int AssignValuesAttrToForm(int idAttr, int idForm)
        {
            string[] paramsArray = new string[] { idAttr.ToString() , idForm.ToString() };

            return Convert.ToInt32(sqliteCmd.ExecuteScalarWithParams(GetQueryWithTwoValues(NameTablesDB.ValuesAttrToForm, "Id_Attribute", "Id_Form"), paramsArray));
        }

        public int InsertImport(string namePerson)
        {
            string[] paramsArray = new string[] { namePerson };

            return Convert.ToInt32(sqliteCmd.ExecuteScalarWithParams(GetQueryWithOneValue(NameTablesDB.Import, "Person"), paramsArray));
        }

        public int InsertImportAttr(string nameAttr)
        {
            string[] paramsArray = new string[] { nameAttr };

            return Convert.ToInt32(sqliteCmd.ExecuteScalarWithParams(GetQueryWithOneValue(NameTablesDB.ImportAttr, "Name"), paramsArray));
        }


        public int InsertImportRow(int idImport)
        {
            string[] paramsArray = new string[] { idImport.ToString() };

            return Convert.ToInt32(sqliteCmd.ExecuteScalarWithParams(GetQueryImportRow(NameTablesDB.ImportRows, "Id_Import"), paramsArray));
        }

        public int InsertImportValue(int idAttr, int idRow, string value)
        {
            string[] paramsArray = new string[] { idAttr.ToString(), idRow.ToString(), value };

            return Convert.ToInt32(sqliteCmd.ExecuteScalarWithParams(GetQueryWithThreeValues(NameTablesDB.ImportValues, "Id_ImportAttr", "Id_Row", "Value"), paramsArray));
        }

        public int InsertImportValue(string nameAttr, int idRow, string value)
        {
            int idAttr = InsertImportAttr(nameAttr);

            return InsertImportValue(idAttr, idRow, value);
        }

        private string GetQueryWithThreeValues(string nameTable, string column1, string column2, string column3)
        {
            return @"INSERT OR IGNORE INTO @TABLE(@column1, @column2, @column3) Values(@param1, @param2, @param3);
            SELECT Id From @TABLE WHERE @column1 = @param1 AND @column2=@param2 AND @column3=@param3 ORDER BY Id DESC LIMIT 1".Replace("@TABLE", nameTable).Replace("@column1", column1).Replace("@column2", column2).Replace("@column3", column3);
        }


        private string GetQueryImportRow(string nameTable, string column1)
        {
            return @"INSERT INTO @TABLE(@column1) Values(@param1);
            SELECT Id From @TABLE WHERE @column1 = @param1 ORDER BY Id DESC LIMIT 1".Replace("@TABLE", nameTable).Replace("@column1", column1);
        }

        private string GetQueryWithOneValue(string nameTable, string column1)
        {
            return @"INSERT OR IGNORE INTO @TABLE(@column1) Values(@param1);
            SELECT Id From @TABLE WHERE @column1 = @param1 ORDER BY Id DESC LIMIT 1".Replace("@TABLE", nameTable).Replace("@column1", column1);
        }

        private string GetQueryWithTwoValues(string nameTable, string column1, string column2)
        {

            return @"INSERT OR IGNORE INTO @TABLE(@column1, @column2) Values(@param1, @param2);
            SELECT Id From @TABLE WHERE @column1 = @param1 AND @column2=@param2 ORDER BY Id DESC LIMIT 1".Replace("@TABLE", nameTable).Replace("@column1", column1).Replace("@column2", column2);

        }

        private string GetQueryAttrFunctionsOrCommentsOrStyles(string nameTable, string column1, string column2)
        {

            return @"UPDATE @TABLE SET @column2=@param2 WHERE @column1 = @param1;
            INSERT OR IGNORE INTO @TABLE(@column1, @column2) Values(@param1, @param2);
            SELECT Id From @TABLE WHERE @column1 = @param1 AND @column2=@param2".Replace("@TABLE", nameTable).Replace("@column1", column1).Replace("@column2", column2);


            //return @"IF NOT EXISTS(Select Id FROM @TABLE WHERE @column1 = @param1)
            //         INSERT INTO @TABLE OUTPUT Inserted.Id Values(@param1, @param2)
            //         ELSE
            //         BEGIN
            //         IF NOT EXISTS(Select Id FROM @TABLE WHERE @column1 = @param1 AND @column2 = @param2)
            //            UPDATE @TABLE SET @column2=@param2 OUTPUT Inserted.Id WHERE @column1 = @param1
            //         ELSE
            //            Select Id FROM @TABLE WHERE @column1 = @param1 AND @column2 = @param2
            //         END".Replace("@TABLE", nameTable).Replace("@column1", column1).Replace("@column2", column2);
        }
    }
}
