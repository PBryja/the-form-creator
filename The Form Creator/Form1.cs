﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Data.SQLite;
using System.IO;
using System.Text;
using System.Linq;

namespace The_Form_Creator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();


        }



        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
                {
                    new FillerBox().FillListBoxWithForms(listBox1, conDB);
                }
            }
            catch
            {
                MessageBox.Show("Program zostanie wyłączony");
                System.Windows.Forms.Application.Exit();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form6(), this);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form2(), this);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!BoxAdapter.IsListBoxSelected(listBox1))
            {
                MessageBox.Show("Wybierz formatkę, którą chcesz wygenerować!");
                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            int idForm = Convert.ToInt32(listBox1.SelectedValue);

            SQLiteEasierCommands sqliteCmd = new SQLiteEasierCommands(new SQLiteConnector().getSQLiteConnection());
            SQLiteDataReader reader = sqliteCmd.GetExecutedReader("SELECT AttributeToTheForm.Id_Attribute From AttributeToTheForm INNER JOIN Attributes ON Attributes.Id = AttributeToTheForm.Id_Attribute WHERE AttributeToTheForm.Id_Form=" + idForm + " ORDER BY Attributes.Position");


            if (!reader.HasRows)
            {
                MessageBox.Show("Brak przypisanych atrybutów do wybranej formatki");
                reader.Close();
                return;
            }

            List<Attribute> attributesList = new List<Attribute>();

            while (reader.Read())
            {
                Attribute attribute = new Attribute();
                attribute.id = Convert.ToInt32(reader[0]);
                attributesList.Add(attribute);
            }

            reader.Close();

            foreach (Attribute attr in attributesList)
            {
                GetterValuesAttr getValAttr = new GetterValuesAttr(sqliteCmd);

                attr.name = getValAttr.GetName(attr.id);
                attr.function = getValAttr.GetFunction(attr.id, idForm);
                attr.comment = getValAttr.GetComment(attr.id, idForm);
                attr.color = getValAttr.GetStyle(attr.id);
            }

            Dictionary<string, string> dict = new Dictionary<string, string>();

            for (int i = 0; i < attributesList.Count; i++)
            {
                if (attributesList[i].function.IndexOf("@") > -1)
                {
                    Regex regex = new Regex("@(.+?)@");

                    foreach (Match match in regex.Matches(attributesList[i].function))
                        if (!dict.ContainsKey(match.ToString().Replace("@", "")))
                            dict.Add(match.ToString().Replace("@", ""), ""); // dic[id,""]


                }
            }

            Dictionary<string, string> dict2 = new Dictionary<string, string>();
            string alph = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; //26 znakow 0-25

            for (int i = 0; i < attributesList.Count; i++)
            {
                int laps = (i) / 26;
                foreach (var item in dict)
                {
                    if (attributesList[i].id == Convert.ToInt32(item.Key.Replace("$", "").Replace(":", "")))
                    {
                        string temp = "";
                        if (laps > 0)
                            temp += alph[laps - 1];

                        temp += alph[i - (laps * 26)];
                        if (item.Key.Contains("$"))
                            dict2.Add(item.Key, temp + "$2");
                        else if (item.Key.Contains(":"))
                            dict2.Add(item.Key, temp);
                        else
                            dict2.Add(item.Key, temp + "2");

                    }
                }
            }

            foreach (var item in dict2)
            {
                for (int i = 0; i < attributesList.Count; i++)
                {
                    string part = "@" + item.Key + "@";
                    if (attributesList[i].function.IndexOf(part) > -1)
                    {
                        attributesList[i].function = attributesList[i].function.Replace(part, item.Value);
                    }
                }

            }

            Microsoft.Office.Interop.Excel.Application oXL;
            Microsoft.Office.Interop.Excel.Workbook oWB;
            Microsoft.Office.Interop.Excel.Worksheet oSheet;
            Microsoft.Office.Interop.Excel.Range oRng2;

            oXL = new Microsoft.Office.Interop.Excel.Application();

            var workBooks = oXL.Workbooks;

            oWB = (Microsoft.Office.Interop.Excel.Workbook)workBooks.Add("");
            oSheet = (Microsoft.Office.Interop.Excel.Worksheet)oWB.ActiveSheet;

            int maxRows = 2;
            for (int i = 0; i <= maxRows; i++)
            {
                for (int j = 0; j < attributesList.Count; j++)
                {
                    if (i == 0)
                    {
                        oSheet.Cells[i + 1, j + 1] = attributesList[j].name;
                        if (attributesList[j].color.Length > 0)
                        {
                            oSheet.Cells[i + 1, j + 1].Interior.Color = System.Drawing.ColorTranslator.FromHtml(attributesList[j].color);
                            oSheet.Cells[i + 2, j + 1].Interior.Color = System.Drawing.ColorTranslator.FromHtml(attributesList[j].color);
                        }
                    }

                    else if (i > 0 && i < maxRows)
                    {
                        if (attributesList[j].function.Contains("@"))
                            attributesList[j].function = attributesList[j].function.Replace("@", "BRAK_ID");
                        try
                        {
                            oSheet.Cells[i + 1, j + 1].FormulaLocal = attributesList[j].function;

                        }
                        catch
                        {
                            oSheet.Cells[i + 1, j + 1].FormulaLocal = "Błędna formuła";
                        }
                    }

                    else if (i == maxRows)
                    {
                        oSheet.Cells[i + 20, j + 1] = attributesList[j].comment;
                    }
                }
            }

            oSheet.get_Range("A22", "A22").EntireRow.Cells.ColumnWidth = 20;
            oSheet.get_Range("A22", "A22").EntireRow.Cells.Font.Size = 8;
            oSheet.get_Range("A22", "A22").EntireRow.Cells.Font.Name = "Calibri";

            oSheet.get_Range("A1", "A1").EntireRow.Cells.Font.Color = System.Drawing.Color.Red;
            oSheet.get_Range("A1", "A1").EntireRow.Cells.Font.Color = System.Drawing.Color.Red;
            oSheet.get_Range("A1", "A1").EntireRow.Cells.Font.Bold = true;
            oSheet.get_Range("A1", "A1").EntireRow.Cells.Font.Size = 9;
            oSheet.get_Range("A1", "A1").EntireRow.Cells.Font.Name = "Calibri";
            oSheet.get_Range("A1", "A1").EntireRow.Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

            oSheet.get_Range("A2", "A2").EntireRow.Cells.Font.Size = 8;
            oSheet.get_Range("A2", "A2").EntireRow.Cells.Font.Name = "Calibri";
            oSheet.get_Range("A2", "A2").EntireRow.Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oSheet.Columns.AutoFit();
            oSheet.Columns.Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            oSheet.Columns.Cells.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignTop;

            oSheet.get_Range("A20", "A20").EntireRow.Cells.Interior.Color = System.Drawing.ColorTranslator.FromHtml("#BFBFBF");
            oSheet.get_Range("A21", "A21").EntireRow.Cells.Interior.Color = System.Drawing.ColorTranslator.FromHtml("#D9D9D9");
            oSheet.get_Range("A22", "A22").EntireRow.Cells.Interior.Color = System.Drawing.ColorTranslator.FromHtml("#f2f2f2");

            oRng2 = oSheet.get_Range("A2", "F2").EntireRow;

            oRng2.Copy(oSheet.get_Range("A3", "A19"));

            Cursor.Current = Cursors.Default;

            oXL.Visible = true;
            Marshal.ReleaseComObject(oWB);
            Marshal.ReleaseComObject(oXL);
            Marshal.ReleaseComObject(workBooks);
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            System.Windows.Forms.Application.Exit();
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            button2_Click(sender, e);
        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form14(), this);
        }
    }
}
