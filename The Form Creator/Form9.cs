﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
namespace The_Form_Creator
{
    public partial class Form9 : Form
    {
        static public string selectedAttrName;
        static public string selectedFormName;
        private string textError;

        public Form9()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form6(), this);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
            {

                SQLiteEasierCommands sqliteCmd = new SQLiteEasierCommands(conDB);
                Attribute attr = new Attribute();
                GetterValuesAttr getterAttr = new GetterValuesAttr(sqliteCmd);

                attr.name = ((DataRowView)listBox1.SelectedItem).Row[1].ToString();
                attr.id = Convert.ToInt32(listBox1.SelectedValue);
                attr.position = getterAttr.GetPosition(attr.id);
                if (attr.position < 0)
                {
                    MessageBox.Show("Atrybut nie istnieje. Spróbuj odświeżyc dane.");
                    return;
                }

                numericUpDown1.Value = attr.position;
                textBox3.Text = attr.name;

                if (checkBox1.Checked)
                {
                    for (int i = 0; i < checkedListBox1.Items.Count; i++)
                        checkedListBox1.SetItemChecked(i, false);

                    List<string> listToCheck = getterAttr.GetAllFormsAssignedTo(attr.id);

                    if (listToCheck != null)
                    {
                        for (int i = 0; i < checkedListBox1.Items.Count; i++)
                        {
                            if (listToCheck.Contains(checkedListBox1.Items[i].ToString()))
                                checkedListBox1.SetItemChecked(i, true);
                            else
                                checkedListBox1.SetItemChecked(i, false);
                        }
                    }
                }
                button2.Enabled = true;
                textBox3.Enabled = true;
                button4.Enabled = true;
                button4.PerformClick();
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void clearStatusStrip1()
        {
            statusStrip1.Items[0].Text = "";
            popKomToolStripMenuItem.Text = textBox2.Text;
            popFuncToolStripMenuItem.Text = textBox1.Text;

            if (textBox2.TextLength > 0)
                commentStatusStrip1.Visible = true;

            else
                commentStatusStrip1.Visible = false;

            if (textBox1.TextLength > 0)
                funcStatusStrip1.Visible = true;
            else
                funcStatusStrip1.Visible = false;

            if (textBox2.TextLength > 0 || textBox1.TextLength > 0)
                statusStrip1.Visible = true;
            else
                statusStrip1.Visible = false;


        }

        private void button4_Click(object sender, EventArgs e)
        {
            List<string> listCheckedItems = checkedListBox1.CheckedItems.OfType<string>().ToList();

            if (listCheckedItems.Count == 0)
                return;

            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
            {
                clearStatusStrip1();
                textBox1.Text = "";
                textBox2.Text = "";

                StatusStripAdapter stripAdapter = new StatusStripAdapter();
                SQLiteEasierCommands sqliteCmd = new SQLiteEasierCommands(conDB);
                GetterValuesAttr getterValuesAttr = new GetterValuesAttr(sqliteCmd);

                string nameFormList = listCheckedItems[0];
                string nameAttr = ((DataRowView)listBox1.SelectedItem).Row[1].ToString();
                int idAttr = getterValuesAttr.GetIdByName(nameAttr);

                string firstFunctionString = getterValuesAttr.GetFunction(idAttr, nameFormList);

                if (!(firstFunctionString.Length > 0))
                    stripAdapter.AddMessage(1);

                string firstCommentString = getterValuesAttr.GetComment(idAttr, nameFormList);

                if (!(firstCommentString.Length > 0))
                    stripAdapter.AddMessage(2);

                if (listCheckedItems.Count > 1)
                {
                    for (int i = 1; i < listCheckedItems.Count(); i++)
                    {
                        string nameOfNextForm = listCheckedItems[i];
                        string excelFunction = getterValuesAttr.GetFunction(idAttr, nameOfNextForm);

                        if (!(excelFunction.Length > 0))
                            stripAdapter.AddMessage(1);
                        else if (!firstFunctionString.Equals(excelFunction))
                            stripAdapter.AddMessage(3);
                    }

                    for (int i = 1; i < listCheckedItems.Count(); i++)
                    {
                        string nameOfNextForm = listCheckedItems[i];
                        string excelComment = getterValuesAttr.GetComment(idAttr, nameOfNextForm);

                        if (!(excelComment.Length > 0))
                            stripAdapter.AddMessage(2);
                        else if (!firstCommentString.Equals(excelComment))
                            stripAdapter.AddMessage(4);
                    }
                }

                if (!stripAdapter.AreFunctionsDifferent())
                    textBox1.Text = firstFunctionString;

                if (!stripAdapter.AreCommentsDifferent())
                    textBox2.Text = firstCommentString;

                statusStrip1.Items[0].Text = stripAdapter.GenerateMessage();

                if (statusStrip1.Items[0].Text.Length > 0 || commentStatusStrip1.Visible || funcStatusStrip1.Visible)
                {
                    statusStrip1.Visible = true;

                    if (textBox2.Text.Equals(popKomToolStripMenuItem.Text) && commentStatusStrip1.Visible)
                        commentStatusStrip1.Visible = false;

                    if (textBox1.Text.Equals(popFuncToolStripMenuItem.Text) && funcStatusStrip1.Visible)
                        funcStatusStrip1.Visible = false;

                    if (!funcStatusStrip1.Visible && !commentStatusStrip1.Visible && statusStrip1.Items[0].Text.Length < 1)
                        statusStrip1.Visible = false;
                }
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            statusStrip1.Visible = false;

            List<int> saveMethods = checkedListBox2.CheckedIndices.OfType<int>().ToList();

            if (saveMethods.Count == 0)
            {
                MessageBox.Show("Wybierz najpierw sposób zapisu");
                return;
            }

            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
            using (SQLiteTransaction trans = conDB.BeginTransaction())
            {
                try
                {
                    SQLiteEasierCommands sqliteCmd = new SQLiteEasierCommands(conDB);
                    GetterValuesAttr getterAttr = new GetterValuesAttr(sqliteCmd);
                    GetterValuesForm getterForm = new GetterValuesForm(sqliteCmd);
                    Attribute attr = new Attribute();
                    InserterValuesDB inserter = new InserterValuesDB(sqliteCmd);
                    DeleterValuesDB deleter = new DeleterValuesDB(sqliteCmd);

                    attr.id = Convert.ToInt32(listBox1.SelectedValue);


                    if (saveMethods.Contains(0))
                    {
                        attr.name = ((DataRowView)listBox1.SelectedItem).Row[1].ToString();

                        SQLiteDataReader reader = sqliteCmd.GetExecutedReader("SELECT Id, Name From Attributes Where Name LIKE '" + attr.name + "' AND Id=" + attr.id);  //zrobic jakas metode do tego i dodac do inncyh formow
                        if (!reader.HasRows)
                        {
                            MessageBox.Show("Atrybut nie został znaleziony. Prawdopodobnie został usunięty lub nazwa została zmieniona. Odśwież dane.");
                            reader.Close();
                            return;
                        }
                        reader.Close();


                        reader = sqliteCmd.GetExecutedReader("SELECT Name From Attributes Where Name LIKE '" + textBox3.Text.ToString() + "' AND Id!=" + attr.id);  //zrobic jakas metode do tego i dodac do inncyh formow
                        if (reader.HasRows)
                        {
                            MessageBox.Show("Taka nazwa już istnieje.");
                            reader.Close();
                            return;
                        }
                        reader.Close();

                        PositionerAttr positioner = new PositionerAttr(sqliteCmd);

                        int newPosition = Convert.ToInt32(numericUpDown1.Value);
                        attr.position = getterAttr.GetPosition(attr.name);

                        if (attr.position == -1)
                        {
                            MessageBox.Show("Błąd podczas wczytywania pozycji");
                            return;
                        }

                        positioner.Move(attr, newPosition);

                        sqliteCmd.ExecuteNonQuery("UPDATE Attributes SET Name='" + textBox3.Text + "' WHERE Id=" + attr.id);
                        sqliteCmd.ExecuteNonQuery("UPDATE ImportAttr SET Name='" + textBox3.Text + "' WHERE Name LIKE '" + attr.name + "'");

                        if (saveMethods.Count == 1)
                        {
                            trans.Commit();
                            new FillerBox().FillListBoxWithAttrs(listBox1, conDB);

                            listBox1.SelectedValue = attr.id;

                            MessageBox.Show("Edycja zakończona pomyślnie");
                            return;

                        }
                        //////////////pozycja koniec
                    }
                    List<string> listCheckedForms = checkedListBox1.CheckedItems.OfType<string>().ToList();

                    foreach (var item in listCheckedForms)
                    {

                        int idForm = getterForm.GetIdByName(item.ToString());

                        if (idForm == 0)
                        {
                            trans.Rollback();

                            textError = "Wystąpił błąd podczas pobierania formatki. Prawdopodobnie formatka została usunięta lub ma zmienioną nazwę. Spróbuj odświeżyć dane.";
                            LogsManager.saveErrorTxt(textError + " EDYCJA ATTR");
                            MessageBox.Show(textError);
                            return;
                        }

                        if (saveMethods.Contains(1))
                            if (getterForm.GetIdAttrToForm(attr.id, idForm) <= 0)
                                if (inserter.AssignAttrToForm(attr.id, idForm) < 0)
                                {
                                    trans.Rollback();
                                    textError = "Błąd podczas przypisywania atrybutów do formatki";
                                    LogsManager.saveErrorTxt(textError + " EDYCJA ATTR");
                                    MessageBox.Show(textError);
                                    return;
                                }

                        if (saveMethods.Contains(2) || saveMethods.Contains(3))
                        {
                            int idValuesAttrToForm = getterForm.GetIdValuesAttrToForm(attr.id, idForm);

                            if (saveMethods.Contains(2))
                                if (textBox1.Text.Length > 0)
                                {
                                    if (inserter.AssignFunctionToAttrAndForm(attr.id, idForm, textBox1.Text) < 0)
                                    {
                                        trans.Rollback();
                                        textError = "Błąd podczas przypisywania funkcji";
                                        LogsManager.saveErrorTxt(textError + " EDYCJA ATTR");
                                        MessageBox.Show(textError);
                                        return;
                                    }
                                }
                                else
                                {
                                    string excelFunction = getterAttr.GetFunction(attr.id, idForm);
                                    if (excelFunction.Length > 0)
                                        if (!deleter.DeleteAttributeFunctionsNEWByIdValuesAttrToForm(idValuesAttrToForm))
                                        {
                                            trans.Rollback();
                                            textError = "Błąd podczas usuwania funkcji";
                                            LogsManager.saveErrorTxt(textError + " EDYCJA ATTR");
                                            MessageBox.Show(textError);
                                            return;
                                        }
                                }

                            if (saveMethods.Contains(3))
                                if (textBox2.Text.Length > 0)
                                {
                                    if (inserter.AssignCommentToAttrAndForm(attr.id, idForm, textBox2.Text) < 0)
                                    {
                                        trans.Rollback();
                                        textError = "Błąd podczas przypisywania komentarza";
                                        LogsManager.saveErrorTxt(textError + " EDYCJA ATTR");
                                        MessageBox.Show(textError);
                                        return;
                                    }
                                }
                                else
                                {
                                    string excelComment = getterAttr.GetComment(attr.id, idForm);
                                    if (excelComment.Length > 0)
                                        if (!deleter.DeleteAttributeCommentsNEWByIdValuesAttrToForm(idValuesAttrToForm))
                                        {
                                            trans.Rollback();
                                            textError = "Błąd podczas usuwania komentarza";
                                            LogsManager.saveErrorTxt(textError + " EDYCJA ATTR");
                                            MessageBox.Show(textError);
                                            return;
                                        }
                                }
                        }

                    }

                    if (saveMethods.Contains(4))
                    {
                        List<string> listAllForms = checkedListBox1.Items.OfType<string>().ToList();

                        foreach (var item in listAllForms)
                        {
                            int idForm = getterForm.GetIdByName(item.ToString());
                            if (idForm == 0)
                            {
                                trans.Rollback();

                                textError = "Wystąpił błąd podczas pobierania formatki. Prawdopodobnie formatka została usunięta lub ma zmienioną nazwę. Spróbuj odświeżyć dane.";
                                LogsManager.saveErrorTxt(textError + " EDYCJA ATTR");
                                MessageBox.Show(textError);
                                return;
                            }
                            int idAttrToForm = getterForm.GetIdAttrToForm(attr.id, idForm);

                            if (idAttrToForm > 0 && !listCheckedForms.Contains(item))
                            {
                                if (!deleter.DeleteAttributeToTheForm(attr.id, idForm))
                                {
                                    trans.Rollback();
                                    textError = "Błąd podczas oddzielania atrybutu od formatki";
                                    LogsManager.saveErrorTxt(textError + " EDYCJA ATTR");
                                    MessageBox.Show(textError);
                                    return;
                                }
                            }

                        }

                    }

                    trans.Commit();
                    new FillerBox().FillListBoxWithAttrs(listBox1, conDB);

                    listBox1.SelectedValue = attr.id;

                    MessageBox.Show("Edycja zakończona pomyślnie");
                }
                catch (Exception exc)
                {
                    trans.Rollback();
                    textError = "Nieoczekiwany błąd.\n";

                    LogsManager.saveErrorTxt(textError + exc.Message);
                    MessageBox.Show(textError, "Błąd");
                }
            }

        }

        private void Form9_Load(object sender, EventArgs e)
        {
            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
            {
                SQLiteEasierCommands sqliteCmd = new SQLiteEasierCommands(conDB);

                SQLiteDataReader reader = sqliteCmd.GetExecutedReader("SELECT Name FROM Form");
                new FillerBox().FillListBoxByList(checkedListBox1, new SQLiteReaderToListConverter().getListFromReader(reader));
                reader.Close();

                //new FillerBox().FillBoxBySQLiteDataAdapter(attributesBindingSource1, new SQLiteDataAdapter("SELECT Id,Name FROM Attributes ORDER BY Position", new SQLiteConnector().getSQLiteConnection()));
                new FillerBox().FillListBoxWithAttrs(listBox1, conDB);
                new FillerBox().FillComboBoxWithAttrs(comboBox1, conDB);

                reader = sqliteCmd.GetExecutedReader("SELECT Position FROM Attributes ORDER BY Position Desc LIMIT 1;");
                reader.Read();
                if (!reader.HasRows)
                {
                    reader.Close();
                    return;
                }
                numericUpDown1.Maximum = Convert.ToInt32(reader[0]);
                reader.Close();

            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (comboBox1.Items.Count > 0)
            {
                textBox1.Paste("@" + comboBox1.SelectedValue.ToString() + "@");
            }

        }


        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            Application.Exit();
        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            button3.Enabled = true;
            textBox3.Enabled = true;
            button4.Enabled = true;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            List<int> listItems = new List<int>();

            foreach (Object item in checkedListBox1.Items)
            {
                int index = checkedListBox1.Items.IndexOf(item);
                listItems.Add(index);
            }

            foreach (var item in listItems)
            {
                if (checkedListBox1.GetItemCheckState(item) == CheckState.Checked)
                    checkedListBox1.SetItemChecked(item, false);
                else
                    checkedListBox1.SetItemChecked(item, true);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (listBox1.SelectedIndex > -1)
            {
                button3.Enabled = true;

                if (!((DataRowView)listBox1.SelectedItem).Row[1].ToString().Equals(textBox3.Text))
                {
                    button2.Enabled = false;
                    button4.Enabled = false;
                }
                else
                {
                    button2.Enabled = true;
                    button4.Enabled = true;
                }
            }
        }

        private void Form9_ResizeEnd(object sender, EventArgs e)
        {
            if (Width <= 645)
            {
                button6.Font = new Font(button6.Font.FontFamily, 7);
                button6.Text = "Odwróć";

                button4.Font = new Font(button4.Font.FontFamily, 10, button4.Font.Style);
                label7.Font = new Font(label7.Font.FontFamily, 10, label7.Font.Style);
                label3.Font = new Font(label3.Font.FontFamily, 9, label3.Font.Style);
                label7.Text = "Poz.";
                button3.Font = new Font(button3.Font.FontFamily, 9, button3.Font.Style);
            }
            else if (Width > 645)
            {
                button3.Font = new Font(button3.Font.FontFamily, 14, button3.Font.Style);
                button6.Font = new Font(button6.Font.FontFamily, 9);
                button6.Text = "Odwróć zaznaczenie";
                button4.Font = new Font(button4.Font.FontFamily, 14, button4.Font.Style);
                label7.Font = new Font(label7.Font.FontFamily, 12, label7.Font.Style);
                label3.Font = new Font(label3.Font.FontFamily, 12, label3.Font.Style);
                label7.Text = "Pozycja";
            }

            TableLayoutColumnStyleCollection styles = tableLayoutPanel10.ColumnStyles;
            if (Width <= 578)
            {
                checkBox1.Text = "";
                styles[1].SizeType = SizeType.Percent;
                styles[1].Width = 80;
                styles[0].SizeType = SizeType.Percent;
                styles[0].Width = 20;
                //tableLayoutPanel10.GetColumn();
            }
            else if (Width > 578)
            {
                styles[1].SizeType = SizeType.Percent;
                styles[1].Width = 60;
                styles[0].SizeType = SizeType.Percent;
                styles[0].Width = 40;
                checkBox1.Text = "Zaznacz formatki";
            }


        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (textBox3.TextLength > 2 && button3.Enabled && button4.Enabled)
                button2.Enabled = true;
            else if (textBox3.TextLength <= 2 && button3.Enabled)
                button2.Enabled = false;
        }

        private void popFunToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Text = popFuncToolStripMenuItem.Text;
        }

        private void poprzedniKomentarzToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void popKomToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox2.Text = popKomToolStripMenuItem.Text;
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void popFuncToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Text = popFuncToolStripMenuItem.Text;
        }

        private void popKomToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            textBox2.Text = popKomToolStripMenuItem.Text;
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex > -1)
            {
                selectedAttrName = ((DataRowView)listBox1.SelectedItem).Row[1].ToString();
                Form10 form = new Form10();
                form.Show();
            }

        }

        private void checkedListBox1_DoubleClick(object sender, EventArgs e)
        {
            if (checkedListBox1.SelectedIndex > -1)
            {
                selectedFormName = checkedListBox1.SelectedItem.ToString();
                Form11 form = new Form11();
                form.Show();
            }
        }

        private void listBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && button3.Enabled)
            {
                button3.PerformClick();
            }
        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        void comboBox1_MouseWheel(object sender, MouseEventArgs e)
        {
            ((HandledMouseEventArgs)e).Handled = true;
        }
    }
}
