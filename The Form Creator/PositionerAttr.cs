﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace The_Form_Creator
{
    class PositionerAttr
    {
        private SQLiteEasierCommands sqliteCmd;
        private List<int> listAllPositions;

        public PositionerAttr(SQLiteEasierCommands sqliteCmd)
        {
            this.sqliteCmd = sqliteCmd;
        }

        public int GetLastPosition()
        {
            SQLiteDataReader reader = sqliteCmd.GetExecutedReader("SELECT Position FROM Attributes ORDER BY Position Desc LIMIT 1");
            reader.Read();
            int result = (reader.HasRows) ? Convert.ToInt32(reader[0]) : 1;
            reader.Close();

            return result;
        }

        public List<int> GetAllPositions()
        {
            List<int> listOfPositions = new List<int>();
            SQLiteDataReader reader = sqliteCmd.GetExecutedReader("SELECT Position From Attributes");

            if (reader.HasRows)
                while (reader.Read())
                    listOfPositions.Add(Convert.ToInt32(reader[0]));
            reader.Close();
            return listOfPositions;
        }

        public void PreparePositionsToSwap(int beginPos, int endPos)
        {
            foreach (int item in listAllPositions)
                if (!(item > beginPos && item > endPos) && !(item < beginPos && item < endPos))
                    sqliteCmd.ExecuteNonQuery("UPDATE Attributes SET Position='" + (item * -1) + "' WHERE Position=" + item);
        }

        public bool PrepareToMove(int idAttr, int newPos)
        {
            this.listAllPositions = GetAllPositions();

            if (!listAllPositions.Contains(newPos))
            {
                MoveToPos(idAttr, newPos);
                return false;
            }
            else
                return true;
        }

        public bool Move(Attribute attr, int newPosition)
        {
            if (attr.position == newPosition)
                return true;

            if (!PrepareToMove(attr.id, newPosition))
                return true;

            PreparePositionsToSwap(attr.position, newPosition);

            if (attr.position < newPosition)
                MoveDownOtherPositions(attr.position, newPosition);
            else
                MoveUpOtherPositions(attr.position, newPosition);

            MoveToPos(attr.id, newPosition);

            return true;
        }

        public bool MoveNewAttribute(int idAttr, int position)
        {
            if (!PrepareToMove(idAttr, position))
                return true;

            int lastPosition = GetLastPosition();

            PreparePositionsToSwap(position, lastPosition);

            MoveUpOtherPositions(position-1, lastPosition);
            MoveToPos(idAttr, position);

            return true;

        }

        private void MoveToPos(int idAttr, int pos)
        {
            sqliteCmd.ExecuteNonQuery("UPDATE Attributes SET Position='" + pos + "' WHERE Id=" + idAttr);
        }

        public void MoveDownOtherPositions(int beginPos, int endPos)
        {
            foreach (var item in listAllPositions)
                if (!(item > endPos && item >= beginPos) && !(item < endPos && item <= beginPos))
                    sqliteCmd.ExecuteNonQuery("UPDATE Attributes SET Position='" + (item - 1) + "' WHERE Position=" + (item * -1));
        }

        public void MoveUpOtherPositions(int beginPos, int endPos)
        {
            foreach (var item in listAllPositions)
                if (!(item > endPos && item >= beginPos) && !(item < endPos && item <= beginPos))
                    sqliteCmd.ExecuteNonQuery("UPDATE Attributes SET Position='" + (item + 1) + "' WHERE Position=" + (item * -1));
        }
    }
}
