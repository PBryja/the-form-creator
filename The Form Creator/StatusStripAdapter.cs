﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Form_Creator
{
    class StatusStripAdapter
    {
        private string pick1 = "";
        private string pick2 = "";
        private string pick3 = "";
        private string pick4 = "";

        public void AddMessage(byte pick)
        {
            switch (pick)
            {
                case 1:
                    pick1 = "Przynajmniej w jednej z wybranych formatek jest nieprzypisana funkcja\n";
                    break;
                case 2:
                    pick2 = "Przynajmniej w jednej z wybranych formatek jest nieprzypisany komentarz\n";
                    break;
                case 3:
                    pick3 = "Funkcje różnią się w wybranych formatkach\n";
                    break;
                case 4:
                    pick4 = "Komentarze różnią się w poszczególnych formatkach\n";
                    break;
            }
        }

        public bool AreFunctionsDifferent()
        {
            if (pick1.Length > 0 || pick3.Length > 0)
                return true;
            else
                return false;
        }

        public bool AreCommentsDifferent()
        {
            if (pick2.Length > 0 || pick4.Length > 0)
                return true;
            else
                return false;
        }

        public string GenerateMessage()
        {
            string message = "";
            message += pick1 + pick2 + pick3 + pick4;
            if (message.Length>0)
                message = message.Remove(message.Length - 1);

            return message;
        }

    }
}
