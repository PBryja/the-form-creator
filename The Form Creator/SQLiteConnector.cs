﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SQLite;

namespace The_Form_Creator
{
    class SQLiteConnector
    {
        private SQLiteConnection con;

        public SQLiteConnector()
        {
            try
            {
                con = new SQLiteConnection("Data Source=DB.db;Version=3;Compress=True;");
                con.Open();
                SQLiteCommand sqliteCmd = con.CreateCommand();
                sqliteCmd.CommandText = "PRAGMA foreign_keys = 1";
                sqliteCmd.ExecuteNonQuery();
                createNewTables(sqliteCmd);
               
            }
            catch (Exception exc)
            {
                MessageBox.Show("BŁĄD");
                string errorMessageToUser = MessagesToUser.GetMessage(1);
                MessageBox.Show(errorMessageToUser);
                LogsManager.saveErrorTxt(exc.ToString(), errorMessageToUser, 1);
            }

        }


        private void createNewTables(SQLiteCommand sqliteCmd)
        {
            List<string> queries = new List<string>();

            queries.Add(@"CREATE TABLE IF NOT EXISTS Attributes ( [Id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [Name] NVARCHAR NOT NULL UNIQUE, [Position] INT NOT NULL UNIQUE);");
            queries.Add(@"CREATE TABLE IF NOT EXISTS Form([Id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [Name] NVARCHAR NOT NULL UNIQUE);");
            queries.Add(@"CREATE TABLE IF NOT EXISTS AttributeToTheForm([Id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [Id_Form] INTEGER NOT NULL, [Id_Attribute] INTEGER NOT NULL, 
                            FOREIGN KEY(Id_Form) REFERENCES Form(Id), FOREIGN KEY(Id_Attribute) REFERENCES Attributes(Id), UNIQUE(Id_Form, Id_Attribute));");
            queries.Add(@"CREATE TABLE IF NOT EXISTS AllComments ([Id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [ExcelComment] NVARCHAR NOT NULL UNIQUE);");
            queries.Add(@"CREATE TABLE IF NOT EXISTS AllFunctions ([Id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [ExcelFunction] NVARCHAR NOT NULL UNIQUE);");
            queries.Add(@"CREATE TABLE IF NOT EXISTS AttributeCommentsNEW ([Id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [Id_ValuesAttrToForm] INTEGER NOT NULL, [Id_AllComments] INTEGER NOT NULL,
                            FOREIGN KEY(Id_ValuesAttrToForm) REFERENCES ValuesAttrToForm(Id), FOREIGN KEY(Id_AllComments) REFERENCES AllComments(Id), UNIQUE(Id_ValuesAttrToForm, Id_AllComments));");
            queries.Add(@"CREATE TABLE IF NOT EXISTS AttributeFunctionsNEW ([Id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [Id_ValuesAttrToForm] INTEGER NOT NULL, [Id_AllFunctions] INTEGER NOT NULL,
                            FOREIGN KEY(Id_ValuesAttrToForm) REFERENCES ValuesAttrToForm(Id), FOREIGN KEY(Id_AllFunctions) REFERENCES AllFunctions(Id), UNIQUE(Id_ValuesAttrToForm, Id_AllFunctions));");
            queries.Add(@"CREATE TABLE IF NOT EXISTS StyleAttr([Id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [Id_Attribute] INTEGER NOT NULL UNIQUE, [Color] NVARCHAR NOT NULL, 
                            FOREIGN KEY (Id_Attribute) REFERENCES Attributes(Id), UNIQUE(Id_Attribute,Color));");
            queries.Add(@"CREATE TABLE IF NOT EXISTS ValuesAttrToForm([Id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [Id_Form] INTEGER NOT NULL, [Id_Attribute] INTEGER NOT NULL, 
                            FOREIGN KEY(Id_Form) REFERENCES Form(Id), FOREIGN KEY(Id_Attribute) REFERENCES Attributes(Id), UNIQUE(Id_Form, Id_Attribute));");
            queries.Add(@"CREATE TABLE IF NOT EXISTS Import([Id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [Person] NVARCHAR NOT NULL, [Date] TIMESTAMP DEFAULT (DATETIME(CURRENT_TIMESTAMP, 'LOCALTIME')) NOT NULL);");
            queries.Add(@"CREATE TABLE IF NOT EXISTS ImportAttr([Id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [Name] NVARCHAR NOT NULL UNIQUE);");
            queries.Add(@"CREATE TABLE IF NOT EXISTS ImportRows([Id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [Id_Import] INTEGER NOT NULL,
                            FOREIGN KEY(Id_Import) REFERENCES Import(Id));");
            queries.Add(@"CREATE TABLE IF NOT EXISTS ImportValues([Id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, [Id_ImportAttr] INTEGER NOT NULL, [Id_Row] INTEGER NOT NULL, [Value] NVARCHAR NOT NULL,
                            FOREIGN KEY(Id_ImportAttr) REFERENCES ImportAttr(Id), FOREIGN KEY(Id_Row) REFERENCES ImportRows(Id), UNIQUE(Id_ImportAttr, Id_Row));");


            foreach (var item in queries)
            {
                sqliteCmd.CommandText = item;
                sqliteCmd.ExecuteNonQuery();
            }
        }

        public SQLiteConnector(string connectionString)
        {
            con.ConnectionString = @connectionString;
        }

        public SQLiteConnection getSQLiteConnection()
        {
            return con;
        }

        public void Close2()
        {
            con.Close();
        }
    }
}
