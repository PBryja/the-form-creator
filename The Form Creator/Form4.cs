﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.CheckedListBox;
using System.Data.SQLite;

namespace The_Form_Creator
{


    public partial class Form4 : Form
    {
        private string textError;

        public Form4()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form2(), this);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            checkedListBox1.Items.Clear();

            SQLiteEasierCommands sqlCmd = new SQLiteEasierCommands(new SQLiteConnector().getSQLiteConnection());
            GetterValuesAttr getterAttr = new GetterValuesAttr(sqlCmd);
            GetterValuesForm getterForm = new GetterValuesForm(sqlCmd);

            string nameForm = ((DataRowView)listBox1.SelectedItem).Row[1].ToString();
            textBox1.Text = nameForm;


            List<string> listAllAttributes = getterAttr.GetAllAttributes();
            List<string> listAssignedAttributes = getterForm.GetAllAttributesAssignedTo(nameForm);

            if (listAssignedAttributes == null)
                new FillerBox().FillListBoxByList(checkedListBox1, listAllAttributes);
            else
                new FillerBox().FillCheckedListBoxByList(checkedListBox1, listAllAttributes, listAssignedAttributes);

            button3.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
            {
                SQLiteEasierCommands sqlCmd = new SQLiteEasierCommands(conDB);
                GetterValuesAttr getterAttr = new GetterValuesAttr(sqlCmd);
                GetterValuesForm getterForm = new GetterValuesForm(sqlCmd);
                InserterValuesDB inserter = new InserterValuesDB(sqlCmd);
                DeleterValuesDB deleter = new DeleterValuesDB(sqlCmd);

                string itemSelected = ((DataRowView)listBox1.SelectedItem).Row[1].ToString();
                int idForm = Convert.ToInt32(listBox1.SelectedValue);

                List<string> listAllItems = checkedListBox1.Items.OfType<string>().ToList();
                List<string> listCheckedItems = checkedListBox1.CheckedItems.OfType<string>().ToList();

                using (SQLiteTransaction trans = conDB.BeginTransaction())
                    try
                    {
                        SQLiteDataReader reader = sqlCmd.GetExecutedReader("SELECT Name From Form Where Id=" + idForm);  //zrobic jakas metode do tego i dodac do inncyh formow
                        if (!reader.HasRows)
                        {
                            MessageBox.Show("Formatka nie została znaleziona. Prawdopodobnie została usunięta lub nazwa została zmieniona. Odśwież dane.");
                            reader.Close();
                            return;
                        }
                        reader.Close();

                        sqlCmd.ExecuteNonQuery(new SQLiteCommand("UPDATE Form SET Name='" + textBox1.Text + "' WHERE Id=" + idForm, conDB));

                        foreach (var item in listAllItems)
                        {
                            int idAttrToCheck = getterAttr.GetIdByName(item);
                            int idAttrToForm = getterForm.GetIdAttrToForm(idAttrToCheck, idForm);

                            if (idAttrToCheck <= 0)
                            {
                                trans.Rollback();
                                textError = "Błąd podczas przypisywania lub usuwania atrybutu z formatki. Prawdopodobnie atrybut został usunięty lub ma zmienioną nazwę. Spróbuj odświeżyć stan atrybutów.";
                                LogsManager.saveErrorTxt(textError + " EDYCJA FORM");
                                MessageBox.Show(textError);
                                return;
                            }
                            if (idAttrToForm <= 0 && listCheckedItems.Contains(item))
                            {
                                if (inserter.AssignAttrToForm(idAttrToCheck, idForm) < 0)
                                {
                                    trans.Rollback();
                                    textError = "Błąd podczas przypisywania atrybutów";
                                    LogsManager.saveErrorTxt(textError + " EDYCJA FORM");
                                    MessageBox.Show(textError);
                                    return;
                                }
                            }
                            else if (idAttrToForm > 0 && !listCheckedItems.Contains(item))
                                if (!deleter.DeleteAttributeToTheForm(idAttrToCheck, idForm))
                                {
                                    trans.Rollback();
                                    textError = "Błąd podczas usuwania atrybutów";
                                    LogsManager.saveErrorTxt(textError + " EDYCJA FORM");
                                    MessageBox.Show(textError);
                                    return;
                                }

                        }

                        trans.Commit();
                        MessageBox.Show("Edycja zakończona pomyślnie");
                        new FillerBox().FillListBoxWithForms(listBox1, conDB);
                        listBox1.SelectedValue = idForm;
                    }
                    catch (SQLiteException exc)
                    {
                        if (exc.ErrorCode == 19)
                            textError = "Formatka o danej nazwie już istnieje. Spróbuj zmienić nazwę.\n";
                        else
                            textError = "Błąd podczas dodawania formatki do bazy danych\n";

                        LogsManager.saveErrorTxt(textError + "Błąd zwiazany z SQLiteException" + exc.Message);
                        MessageBox.Show(textError, "Błąd");
                    }
                    catch (Exception exc)
                    {
                        textError = "Błąd podczas dodawania formatki do bazy danych\n";

                        LogsManager.saveErrorTxt(textError + "Błąd nie jest zwiazany z SQLiteException" + exc.Message);
                        MessageBox.Show(textError, "Błąd");
                    }




            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {


            if (listBox1.SelectedIndex > -1)
            {
                button2.Enabled = true;
                if (!((DataRowView)listBox1.SelectedItem).Row[1].ToString().Equals(textBox1.Text))
                    button3.Enabled = false;
                else
                    button3.Enabled = true;
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.TextLength > 2 && !button3.Enabled)
                button3.Enabled = true;
            else if (textBox1.TextLength <= 2 && button3.Enabled)
                button3.Enabled = false;
        }
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            Application.Exit();
        }

        private void Form4_Load(object sender, EventArgs e)
        {

            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
                new FillerBox().FillListBoxWithForms(listBox1, conDB);
        }

    }
}
