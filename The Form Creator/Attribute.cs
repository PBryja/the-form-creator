﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Form_Creator
{
    class Attribute
    {
        public int id { get; set; }
        public string name { get; set; }
        public string comment { get; set; }
        public string function { get; set; }
        public int position { get; set; }
        public string color { get; set; }
    }
}
