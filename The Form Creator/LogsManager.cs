﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Form_Creator
{
    class LogsManager
    {
        static private string[] messages = {
            "#0 Brak przypisanej wiadomości do numeru błędu",
            "#1 Błąd z połączeniem do bazy danych"
        };

        static public void saveErrorTxt(string message1, byte numberOfLog=0)
        {
            string text = createText(message1, numberOfLog);
            saveToTxt(text);
        }

        static public void saveErrorTxt(string message1, string message2, byte numberOfLog=0)
        {
            string text = createText(message1+" "+message2, numberOfLog);
            saveToTxt(text);
        }

        static private string createText(string message, byte number=0)
        {
            string messageLog = getMessage(number);
            string text = "Date: " + DateTime.Now + " " + message + " " + messageLog;
            return text;
        }

        static private string getMessage(byte number)
        {
            if (number > messages.Length - 1)
                number = 0;

            return messages[number];
        }

        static private void saveToTxt(string text)
        {
            using (StreamWriter streamWriter = new StreamWriter("logs.txt", true))
            {  
                streamWriter.WriteLine(text);
            }
        }
    }
}
