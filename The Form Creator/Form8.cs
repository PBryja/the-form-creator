﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Data.SQLite;

namespace The_Form_Creator
{
    public partial class Form8 : Form
    {
        private string textError;

        public Form8()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form6(), this);
        }

        private void Form8_Load(object sender, EventArgs e)
        {
            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
            {

                SQLiteEasierCommands sqliteCmd = new SQLiteEasierCommands(conDB);

                SQLiteDataReader reader = sqliteCmd.GetExecutedReader("SELECT Name FROM Form");
                new FillerBox().FillListBoxByList(checkedListBox1, new SQLiteReaderToListConverter().getListFromReader(reader));
                reader.Close();

                reader = sqliteCmd.GetExecutedReader("SELECT Position FROM Attributes ORDER BY Position Desc LIMIT 1;");
                reader.Read();

                if (!reader.HasRows)
                {
                    reader.Close();
                    return;

                }
                numericUpDown1.Maximum = Convert.ToInt32(reader[0]) + 1;
                reader.Close();


                FillDataGridView(sqliteCmd.GetExecutedReader("SELECT Id,Position,Name FROM Attributes ORDER BY Position"));

            }
        }

        public void FillDataGridView(SQLiteDataReader reader)
        {
            dataGridView1.Rows.Clear();
            while (reader.Read())
                dataGridView1.Rows.Add(reader[0], reader[1], reader[2]);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.TextLength > 2 && !button2.Enabled)
                button2.Enabled = true;
            else if (textBox1.TextLength <= 2 && button2.Enabled)
                button2.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
            using (SQLiteTransaction trans = conDB.BeginTransaction())
            {
                SQLiteEasierCommands sqliteCmd = new SQLiteEasierCommands(conDB);
                InserterValuesDB inserter = new InserterValuesDB(sqliteCmd);
                string nameAttr = textBox1.Text;
                int posAttr = Convert.ToInt32(numericUpDown1.Value);

                SQLiteDataReader reader = sqliteCmd.GetExecutedReader("SELECT Name From Attributes Where Name LIKE '" + nameAttr + "'");  //zrobic jakas metode do tego i dodac do inncyh formow
                if (reader.HasRows)
                {
                    MessageBox.Show("Taka nazwa już istnieje.");
                    reader.Close();
                    return;
                }
                reader.Close();
                try
                {
                    int idAttr = inserter.InsertAttribute(nameAttr, 0);

                    if (idAttr == 0)
                    {
                        trans.Rollback();
                        textError = "Wystąpił nieznany błąd podczas dodawania!";
                        LogsManager.saveErrorTxt(textError + " DODAWANIE ATTR");
                        MessageBox.Show(textError);
                        return;
                    }

                    List<string> listCheckedItems = checkedListBox1.CheckedItems.OfType<string>().ToList();
                    GetterValuesForm getter = new GetterValuesForm(sqliteCmd);
                    PositionerAttr positioner = new PositionerAttr(sqliteCmd);
                    positioner.MoveNewAttribute(idAttr, posAttr);

                    foreach (var item in listCheckedItems)
                    {

                        if (inserter.AssignAttrToForm(idAttr, getter.GetIdByName(item))<0)
                        {
                            trans.Rollback();
                            textError = "Błąd podczas przypisywania atrybutów";
                            LogsManager.saveErrorTxt(textError + " DODAWANIE ATTR");
                            MessageBox.Show(textError);
                            return;
                        }
                    }

                    checkedListBox1.Items.Clear();
                    reader = sqliteCmd.GetExecutedReader("SELECT Name FROM Form");

                    new FillerBox().FillListBoxByList(checkedListBox1, new SQLiteReaderToListConverter().getListFromReader(reader));
                    reader.Close();

                    FillDataGridView(sqliteCmd.GetExecutedReader("SELECT Id,Position,Name FROM Attributes ORDER BY Position"));

                    reader = sqliteCmd.GetExecutedReader("SELECT Position FROM Attributes ORDER BY Position Desc LIMIT 1;");
                    reader.Read();

                    trans.Commit();
                    MessageBox.Show("Atrybut dodany pomyślnie!");

                    if (!reader.HasRows)
                    {
                        reader.Close();
                        return;
                    }

                    numericUpDown1.Maximum = Convert.ToInt32(reader[0]) + 1;
                    reader.Close();

                }
                catch (SQLiteException exc)
                {
                    trans.Rollback();
                    if (exc.ErrorCode == 19)
                        textError = "Atrybut o danej nazwie już istnieje.Spróbuj zmienić nazwę.\n";
                    else
                        textError = "Błąd podczas dodawania atrybutu do bazy danych\n\n";

                    LogsManager.saveErrorTxt(textError + "Błąd zwiazany z SQLiteException\n" + exc.Message);
                    MessageBox.Show(textError, "Błąd");
                }
                catch (Exception exc)
                {
                    trans.Rollback();
                    textError = "Błąd podczas dodawania atrybutu do bazy danych\n ";
                    LogsManager.saveErrorTxt(textError + "Błąd nie jest związany z SqliteException\n" + exc.Message);
                    MessageBox.Show(textError, "Błąd");
                }
            }


        }



        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {

        }

        private void fillByToolStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
        }
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            Application.Exit();
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && button2.Enabled)
            {
                button2.PerformClick();
            }
        }
    }
}
