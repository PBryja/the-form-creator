﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Form_Creator
{
    class GetterValuesForm : GetterFormAndAttr
    {
        public GetterValuesForm(SQLiteEasierCommands sqliteCmd) : base(sqliteCmd)
        {
        }

        public override string GetName(int idForm)
        {
            query = "SELECT Name From Form WHERE Id=" + idForm;
            return GetResult();
        }

        public override int GetIdByName(string nameForm)
        {
            query = "SELECT Id from Form WHERE Name LIKE '" + nameForm + "'";
            return GetResultInt();
        }
        
        public List<string> GetAllAttributesAssignedTo(string nameForm)
        {
            query = @"SELECT Attributes.Name FROM Attributes
                INNER JOIN AttributeToTheForm ON  AttributeToTheForm.Id_Attribute = Attributes.Id
                INNER JOIN Form ON Form.Id = AttributeToTheForm.Id_Form
                WHERE Form.Name = '" + nameForm + "' ORDER BY Attributes.Position";

            return GetResults();
        }
        
        public override List<string> GetComments(string nameForm)
        {
            query = @"SELECT DISTINCT ExcelComment FROM AllComments
				INNER JOIN AttributeCommentsNEW ON AttributeCommentsNEW.Id_AllComments = AllComments.Id
				INNER JOIN ValuesAttrToForm ON ValuesAttrToForm.Id = AttributeCommentsNEW.Id_ValuesAttrToForm 
				INNER JOIN Form ON Form.Id = ValuesAttrToForm.Id_Form
				WHERE Form.Name = '" + nameForm + "'";

            return GetResults();
        }

        public override List<string> GetFunctions(string nameForm)
        {
            query = @"SELECT DISTINCT ExcelFunction FROM AllFunctions
                INNER JOIN AttributeFunctionsNEW ON AttributeFunctionsNEW.Id_AllFunctions = AllFunctions.Id
				INNER JOIN ValuesAttrToForm ON ValuesAttrToForm.Id = AttributeFunctionsNEW.Id_ValuesAttrToForm
				INNER JOIN Form ON Form.Id = ValuesAttrToForm.Id_Form
				WHERE Form.Name = '" + nameForm + "'";

            return GetResults();
        }

        public List<int> GetIdAllAttributesAssignedToForm(int idForm)
        {
            query = @"SELECT AttributeToTheForm.Id_Attribute FROM AttributeToTheForm
                WHERE AttributeToTheForm.Id_Form = "+idForm;

            return GetResultsInt();
        }

        public List<int> GetIdAttrToForm(int idForm)
        {
            query = "SELECT Id From AttributeToTheForm WHERE Id_Form = " + idForm;
            return GetResultsInt();
        }
    }
}
