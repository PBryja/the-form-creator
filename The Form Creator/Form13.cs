﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;


namespace The_Form_Creator
{
    public partial class Form13 : Form
    {
        private string textError;
        public Form13()
        {
            InitializeComponent();
        }

        private void Form13_Load(object sender, EventArgs e)
        {
            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
            {
                new FillerBox().FillListBoxWithForms(listBox1, conDB);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form2(), this);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.TextLength > 2 && !button1.Enabled && listBox1.SelectedIndex > -1)
                button1.Enabled = true;
            else if (textBox1.TextLength <= 2 && button1.Enabled)
                button1.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
            using (SQLiteTransaction trans = conDB.BeginTransaction())
            {
                try
                {

                    SQLiteEasierCommands sqliteCmd = new SQLiteEasierCommands(conDB);
                    GetterValuesAttr getterAttr = new GetterValuesAttr(sqliteCmd);
                    GetterValuesForm getterForm = new GetterValuesForm(sqliteCmd);
                    InserterValuesDB inserter = new InserterValuesDB(sqliteCmd);

                    string itemSelected = ((DataRowView)listBox1.SelectedItem).Row[1].ToString();
                    int idForm = Convert.ToInt32(listBox1.SelectedValue);

                    SQLiteDataReader reader = sqliteCmd.GetExecutedReader("SELECT Id, Name From Form Where Id=" + idForm);  //zrobic jakas metode do tego i dodac do inncyh formow
                    if (!reader.HasRows)
                    {
                        trans.Rollback();
                        MessageBox.Show("Formatka nie została znaleziona. Prawdopodobnie została usunięta lub nazwa została zmieniona. Odśwież dane.");
                        reader.Close();
                        return;
                    }

                    reader.Close();

                    string newName = textBox1.Text;

                    reader = sqliteCmd.GetExecutedReader("SELECT Name From Form Where Name LIKE '" + newName + "'");
                    if (reader.HasRows)
                    {
                        trans.Rollback();
                        MessageBox.Show("Taka nazwa już istnieje.");
                        reader.Close();
                        return;
                    }

                    reader.Close();

                    int idNewForm = inserter.InsertForm(newName);

                    if (idNewForm == 0)
                    {
                        trans.Rollback();
                        textError = "Wystąpił nieznany błąd podczas kopiowania!";
                        LogsManager.saveErrorTxt(textError + " COPY FORM");
                        MessageBox.Show(textError);
                        return;
                    }

                    List<int> listIdAttrToForm = getterForm.GetIdAllAttributesAssignedToForm(idForm);
                    List<int> listIdValuesToForm = new List<int>();
                    List<string> listFunctions = new List<string>();
                    List<string> listComments = new List<string>();

                    foreach (var item in listIdAttrToForm)
                    {
                        if (item > 0)
                        {

                            if (inserter.AssignAttrToForm(item, idNewForm) < 0)
                            {
                                trans.Rollback();
                                textError = "Błąd podczas przypisywania atrybutu do nowej formatki";
                                LogsManager.saveErrorTxt(textError + " COPY FORM");
                                MessageBox.Show(textError);
                                return;
                            }

                            string function = getterAttr.GetFunction(item, idForm);
                            if (function.Length > 0)
                            {
                                if (inserter.AssignFunctionToAttrAndForm(item, idNewForm, function) < 0)
                                {
                                    trans.Rollback();
                                    textError = "Błąd podczas przypisywania funkcji do atrybutu";
                                    LogsManager.saveErrorTxt(textError + " COPY FORM");
                                    MessageBox.Show(textError);
                                    return;
                                }
                            }

                            string comment = getterAttr.GetComment(item, idForm);
                            if (comment.Length > 0)
                            {
                                if (inserter.AssignCommentToAttrAndForm(item, idNewForm, comment) < 0)
                                {
                                    trans.Rollback();
                                    textError = "Błąd podczas przypisywania komentarza do atrybutu";
                                    LogsManager.saveErrorTxt(textError + " COPY FORM");
                                    MessageBox.Show(textError);
                                    return;
                                }
                            }
                        }
                    }
                    trans.Commit();
                    MessageBox.Show("Formatka skopiowana pomyślnie");
                }
                catch(Exception exc)
                {
                    trans.Rollback();
                    string textError = "Błąd podczas kopiowania formatki.";
                    LogsManager.saveErrorTxt(textError + " COPY FORM" + exc.Message);
                    MessageBox.Show(textError);
                }
            }

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex > -1 && textBox1.Text.Length > 2)
            {
                button1.Enabled = true;
            }
            else if (listBox1.SelectedIndex < 0)
                button1.Enabled = false;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            Application.Exit();
        }
    }
}
