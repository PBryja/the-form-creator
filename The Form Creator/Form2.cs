﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace The_Form_Creator
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form1(), this);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form4(), this);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form3(), this);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form5(), this);
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            Application.Exit();
        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form13(), this);

        }
    }
}
