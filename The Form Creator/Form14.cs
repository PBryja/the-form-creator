﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Form_Creator
{
    public partial class Form14 : Form
    {
        public Form14()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form15(), this);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form16(), this);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form1(), this);

        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            Application.Exit();
        }
    }
}
