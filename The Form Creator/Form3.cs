﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Windows.Forms;
using System.Data.SQLite;



namespace The_Form_Creator
{
    public partial class Form3 : Form
    {
        private string textError;

        public Form3()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            FormCloseOpen.FormCloseAndOpen(new Form2(), this);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (SQLiteConnection conDB = new SQLiteConnector().getSQLiteConnection())
            {
                SQLiteEasierCommands sqliteCmd = new SQLiteEasierCommands(conDB);
                InserterValuesDB insert = new InserterValuesDB(sqliteCmd);

                SQLiteDataReader reader = sqliteCmd.GetExecutedReader("SELECT Name From Form Where Name LIKE '" + textBox1.Text.ToString() + "'");  //zrobic jakas metode do tego i dodac do inncyh formow
                if (reader.HasRows)
                {
                    MessageBox.Show("Taka nazwa już istnieje.");
                    reader.Close();

                    return;
                }
                reader.Close();
                using (SQLiteTransaction trans = conDB.BeginTransaction())
                {
                    try
                    {
                        int idForm = insert.InsertForm(textBox1.Text);

                        if (idForm == 0)
                        {
                            trans.Rollback();
                            textError = "Wystąpił nieznany błąd podczas dodawania!";
                            LogsManager.saveErrorTxt(textError + " DODAWANIE FORM");
                            MessageBox.Show(textError);
                            return;
                        }

                        List<string> listCheckedItems = checkedListBox1.CheckedItems.OfType<string>().ToList();
                        GetterValuesAttr getter = new GetterValuesAttr(sqliteCmd);

                        foreach (var item in listCheckedItems)
                        {
                            if (insert.AssignAttrToForm(getter.GetIdByName(item), idForm) < 0)
                            {
                                trans.Rollback();
                                textError = "Błąd podczas przypisywania atrybutów";
                                LogsManager.saveErrorTxt(textError + " DODAWANIE FORM");
                                MessageBox.Show(textError);
                                return;
                            }
                        }
                        trans.Commit();
                        MessageBox.Show("Formatka dodana pomyślnie!");
                    }
                    catch (SQLiteException exc)
                    {
                        trans.Rollback();
                        if (exc.ErrorCode == 19)
                            textError = "Formatka o danej nazwie już istnieje.Spróbuj zmienić nazwę.\n";
                        else
                            textError = "Błąd podczas dodawania formatki do bazy danych\n\n";

                        LogsManager.saveErrorTxt(textError + "Błąd zwiazany z SQLiteException\n" + exc.Message);
                        MessageBox.Show(textError, "Błąd");
                    }
                    catch (Exception exc)
                    {
                        trans.Rollback();
                        textError = "Błąd podczas dodawania formatki do bazy danych\n ";
                        LogsManager.saveErrorTxt(textError + "Błąd nie jest związany z SqliteException\n" + exc.Message);
                        MessageBox.Show(textError, "Błąd");
                    }
                }

            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.TextLength > 2 && !button2.Enabled)
                button2.Enabled = true;
            else if (textBox1.TextLength <= 2 && button2.Enabled)
                button2.Enabled = false;
        }
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            Application.Exit();
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_DragEnter(object sender, DragEventArgs e)
        {

        }

        private void Form3_Load(object sender, EventArgs e)
        {
            SQLiteEasierCommands sqliteCmd = new SQLiteEasierCommands(new SQLiteConnector().getSQLiteConnection());
            SQLiteDataReader reader = sqliteCmd.GetExecutedReader("SELECT Name FROM Attributes ORDER BY Position ASC");

            new FillerBox().FillListBoxByList(checkedListBox1, new SQLiteReaderToListConverter().getListFromReader(reader));
        }
    }
}
