﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace The_Form_Creator
{
    class FillerBox
    {

        public void FillCheckedListBoxByList(CheckedListBox box, List<string> list, List<string> listToBeChecked)
        {
            box.BeginUpdate();

            foreach (string item in list)
            {
                if (listToBeChecked.Contains(item))
                    box.Items.Add(item, true);
                else
                    box.Items.Add(item);

            }

            box.EndUpdate();
        }

        public void FillListBoxByList(ListBox box, List<string> list)
        {
            if (list == null)
                return;

            box.BeginUpdate();

            foreach (string item in list)
                box.Items.Add(item);

            box.EndUpdate();
        }

        public void FillBoxBySQLiteDataAdapter (BindingSource bindingSource, SQLiteDataAdapter sqliteDataAdapter)
        {
            DataTable dataTable = new DataTable();
            sqliteDataAdapter.Fill(dataTable);
            bindingSource.DataSource = dataTable;
        }

        public void FillListBoxBySQLiteDataAdapter(BindingSource bindingSource, SQLiteDataAdapter sqliteDataAdapter)
        {
            DataTable dataTable = new DataTable();
            sqliteDataAdapter.Fill(dataTable);
            bindingSource.DataSource = dataTable;
        }

        public void FillListBoxWithForms(ListBox listBox, SQLiteConnection conDB)
        {
            SQLiteDataAdapter adapter = new SQLiteDataAdapter("SELECT Id, Name FROM Form", conDB);
            DataTable dTable = new DataTable();
            adapter.Fill(dTable);

            listBox.ValueMember = "Id";
            listBox.DisplayMember = "Name";
            listBox.DataSource = dTable;
        }

        public void FillListBoxWithAttrs(ListBox listBox, SQLiteConnection conDB)
        {
            SQLiteDataAdapter adapter = new SQLiteDataAdapter("SELECT Id, Name FROM Attributes ORDER BY Position", conDB);
            DataTable dTable = new DataTable();
            adapter.Fill(dTable);

            listBox.ValueMember = "Id";
            listBox.DisplayMember = "Name";
            listBox.DataSource = dTable;
        }

        public void FillComboBoxWithDates(ComboBox comboBox, SQLiteConnection conDB, bool firstDefaultRow = false)
        {
            SQLiteDataAdapter adapter = new SQLiteDataAdapter("SELECT Id, CAST(Date AS TEXT) Date  FROM Import ORDER BY ID Desc", conDB);
            DataTable dTable = new DataTable();
            adapter.Fill(dTable);
            comboBox.ValueMember = "Id";
            comboBox.DisplayMember = "Date";
            if (firstDefaultRow)
                InsertDefaultRow(dTable, "Date");

            comboBox.DataSource = dTable;
        }

        public void FillComboBoxWithPerson(ComboBox comboBox, SQLiteConnection conDB, bool firstDefaultRow = false)
        {
            SQLiteDataAdapter adapter = new SQLiteDataAdapter("SELECT DISTINCT Person FROM Import ORDER BY ID Desc", conDB);
            DataTable dTable = new DataTable();
            adapter.Fill(dTable);

            comboBox.ValueMember = "Id";
            comboBox.DisplayMember = "Person";

            if (firstDefaultRow)
                InsertDefaultRow(dTable, "Person");

            comboBox.DataSource = dTable;

        }


        public void FillComboBoxWithImportAttrs(ComboBox comboBox, SQLiteConnection conDB, bool firstDefaultRow = false)
        {
            SQLiteDataAdapter adapter = new SQLiteDataAdapter("SELECT Id, Name FROM ImportAttr", conDB);
            DataTable dTable = new DataTable();

            adapter.Fill(dTable);

            comboBox.ValueMember = "Id";
            comboBox.DisplayMember = "Name";

            if (firstDefaultRow)
                InsertDefaultRow(dTable, "Name");

            comboBox.DataSource = dTable;

        }

        private void InsertDefaultRow(DataTable dTable, string displayMember)
        {
                DataRow dRow = dTable.NewRow();
                dRow[displayMember] = "Wybierz";
                dTable.Rows.InsertAt(dRow, 0);
        }

        public void FillComboBoxWithAttrs(ComboBox comboBox, SQLiteConnection conDB, bool firstDefaultRow = false)
        {
            SQLiteDataAdapter adapter = new SQLiteDataAdapter("SELECT Id, Name FROM Attributes ORDER BY Position", conDB);
            DataTable dTable = new DataTable();
           
            adapter.Fill(dTable);
    
            comboBox.ValueMember = "Id";
            comboBox.DisplayMember = "Name";

            if (firstDefaultRow)
                InsertDefaultRow(dTable, "Name");

            comboBox.DataSource = dTable;

        }

        public void FillComboBoxWithForms(ComboBox comboBox, SQLiteConnection conDB, bool firstDefaultRow = false)
        {
            SQLiteDataAdapter adapter = new SQLiteDataAdapter("SELECT Id, Name FROM Form", conDB);
            DataTable dTable = new DataTable();

            adapter.Fill(dTable);

            comboBox.ValueMember = "Id";
            comboBox.DisplayMember = "Name";

            if (firstDefaultRow)
                InsertDefaultRow(dTable, "Name");

            comboBox.DataSource = dTable;

        }


        public void FillTextBoxByList(TextBox textBox, List<string> list)
        {
            if (list == null)
                return;

            foreach (string item in list)
                textBox.Text += item + "\r\n--------\r\n";
        }
    }
}
