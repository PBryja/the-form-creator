﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Form_Creator
{
    class MessagesToUser
    {
        static private string[] messages = {
            "#0 Brak wiadomości",
            "#1 Nie udało się połączyć z bazą danych."
        };

        static public string GetMessage(byte num)
        {
            if (num > messages.Length - 1)
                num = 0;
            return messages[num];
        }
    }
}
