﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace The_Form_Creator
{
    class SQLiteEasierCommands
    {
        private SQLiteConnection conDB;

        public SQLiteEasierCommands(SQLiteConnection conDB)
        {
            this.conDB = conDB;
        }

        public SQLiteDataReader GetExecutedReader(string query)
        {
            return GetExecutedReaderBySQLiteCommand(new SQLiteCommand(query, conDB));
        }

        public SQLiteDataReader GetExecutedReaderWithParams(string query, string[,] paramsArray)
        {
            SQLiteCommand command = new SQLiteCommand(query, conDB);
            for (int i = 0; i < paramsArray.Length; i++)
                command.Parameters.Add(new SQLiteParameter(paramsArray[i, 0], paramsArray[i, 1]));

            return GetExecutedReaderBySQLiteCommand(command);
        }

        private SQLiteDataReader GetExecutedReaderBySQLiteCommand(SQLiteCommand command)
        {
            return command.ExecuteReader();
        }


        public bool ExecuteNonQueryWithParams(string query, string[,] paramsArray)
        {
            SQLiteCommand command = new SQLiteCommand(query, conDB);

            for (int i = 0; i < paramsArray.Length / 2; i++)
                command.Parameters.Add(new SQLiteParameter(paramsArray[i, 0], paramsArray[i, 1]));

            return ExecuteNonQuery(command);
        }

        public bool ExecuteNonQueryWithParams(string query, string[] paramsArray)
        {
            return ExecuteNonQuery(PrepareSQLiteCommand(query, paramsArray));

        }

        public Object ExecuteScalarWithParams(string query, string[] paramsArray)
        {
            return PrepareSQLiteCommand(query, paramsArray).ExecuteScalar();


        }

        private SQLiteCommand PrepareSQLiteCommand(string query, string[] paramsArray)
        {
            SQLiteCommand sqliteCommand = new SQLiteCommand(query, conDB);

            for (int i = 0; i < paramsArray.Length; i++)
            {
                string param = "@param" + (i + 1);
                sqliteCommand.Parameters.AddWithValue(param, paramsArray[i]);
            }
            return sqliteCommand;
        }

        public bool InsertNonQuery(string table, string values)
        {
            return ExecuteNonQuery("Insert Into " + table + " VALUES(" + values + ")");
        }

        public bool UpdateNonQuery(string table, string set, string where)
        {
            return ExecuteNonQuery("UPDATE " + table + " SET " + set + " WHERE " + where);
        }

        public bool DeleteNonQuery(string table, string where)
        {
            return ExecuteNonQuery("Delete From " + table + " WHERE " + where);
        }


        public bool ExecuteNonQuery(SQLiteCommand sqliteCommand)
        {
            int result = sqliteCommand.ExecuteNonQuery();
            sqliteCommand.Cancel();
            return result > 0 ? true : false;
        }



        public bool ExecuteNonQuery(string query)
        {
            SQLiteCommand sqliteCommand = new SQLiteCommand(query, conDB);
            return ExecuteNonQuery(sqliteCommand);
        }

        public Object ExecuteScalar(string query)
        {
            SQLiteCommand sqliteCommand = new SQLiteCommand(query, conDB);
            return sqliteCommand.ExecuteScalar();
        }
    }
}
