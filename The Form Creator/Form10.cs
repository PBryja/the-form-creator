﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Form_Creator
{
    public partial class Form10 : Form
    {
        public Form10()
        {

            InitializeComponent();
        }

        private void Form10_Load(object sender, EventArgs e)
        {
            //Form callingForm;
            // Form1 mainForm = callingForm as Form1;
            
        }

        private void Form10_Load_1(object sender, EventArgs e)
        {
            SQLiteEasierCommands sqliteCmd = new SQLiteEasierCommands(new SQLiteConnector().getSQLiteConnection());
            GetterValuesAttr getterValuesAttr = new GetterValuesAttr(sqliteCmd);

            List<string> attrFunctions = getterValuesAttr.GetFunctions(Form9.selectedAttrName);
            List<string> attrComments = getterValuesAttr.GetComments(Form9.selectedAttrName);

            FillerBox fillerBox = new FillerBox();

            fillerBox.FillTextBoxByList(textBox2, attrFunctions);
            fillerBox.FillTextBoxByList(textBox1, attrComments);

            label3.Text = Form9.selectedAttrName;
            //new FillerBox().FillListBoxByList(listBox2, attrFunctions);
            //new FillerBox().FillListBoxByList(listBox2, attrComments);

        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
        }
    }
}
