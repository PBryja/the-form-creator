﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Form_Creator
{
    class GetterValuesAttr : GetterFormAndAttr
    {
        

        public GetterValuesAttr(SQLiteEasierCommands sqliteCmd) : base(sqliteCmd)
        {
        }

        public override string GetName(int idAttr)
        {
            query = "SELECT Name From Attributes WHERE Id=" + idAttr;
            return GetResult();
        }

        public string GetFunction(int idAttr, int idForm)
        {
            query = @"SELECT AllFunctions.ExcelFunction From AllFunctions 
                    INNER JOIN AttributeFunctionsNEW ON AttributeFunctionsNEW.Id_AllFunctions = AllFunctions.Id
                    INNER JOIN ValuesAttrToForm ON ValuesAttrToForm.Id = AttributeFunctionsNEW.Id_ValuesAttrToForm 
                    WHERE ValuesAttrToForm.Id_Attribute = " + idAttr + " AND ValuesAttrToForm.Id_Form = " + idForm;

            return GetResult();
        }


        public string GetFunction(int idAttr, string nameForm)
        {
            query = @"SELECT AllFunctions.ExcelFunction FROM AllFunctions
                INNER JOIN AttributeFunctionsNEW ON AttributeFunctionsNEW.Id_AllFunctions = AllFunctions.Id
                INNER JOIN ValuesAttrToForm ON ValuesAttrToForm.Id = AttributeFunctionsNEW.Id_ValuesAttrToForm
                INNER JOIN Form ON Form.Id = ValuesAttrToForm.Id_Form
                WHERE ValuesAttrToForm.Id_Attribute = " + idAttr +" AND Form.Name LIKE '" + nameForm + "'";

            return GetResult();
        }

        public string GetStyle(int idAttr)
        {
            query = @"SELECT Color FROM StyleAttr WHERE Id_Attribute="+idAttr;

            return GetResult();
        }

        public override List<string> GetFunctions(string nameAttr)
        {


            query = @"SELECT DISTINCT ExcelFunction FROM AllFunctions
				INNER JOIN AttributeFunctionsNEW ON AttributeFunctionsNEW.Id_AllFunctions = AllFunctions.Id
				INNER JOIN ValuesAttrToForm ON ValuesAttrToForm.Id = AttributeFunctionsNEW.Id_ValuesAttrToForm 
				INNER JOIN Attributes ON Attributes.Id = ValuesAttrToForm.Id_Attribute
				WHERE Attributes.Name LIKE '" + nameAttr + "'";

            return GetResults();
        }

        public override List<string> GetComments(string nameAttr)
        {
            query = @"SELECT DISTINCT ExcelComment FROM AllComments
				INNER JOIN AttributeCommentsNEW ON AttributeCommentsNEW.Id_AllComments = AllComments.Id
				INNER JOIN ValuesAttrToForm ON ValuesAttrToForm.Id = AttributeCommentsNEW.Id_ValuesAttrToForm 
				INNER JOIN Attributes ON Attributes.Id = ValuesAttrToForm.Id_Attribute
				WHERE Attributes.Name LIKE '" + nameAttr + "'";

            return GetResults();
        }

        public string GetComment(int idAttr, string nameForm)
        {
            query = @"SELECT AllComments.ExcelComment FROM AllComments
                INNER JOIN AttributeCommentsNEW ON AttributeCommentsNEW.Id_AllComments = AllComments.Id
                INNER JOIN ValuesAttrToForm ON ValuesAttrToForm.Id = AttributeCommentsNEW.Id_ValuesAttrToForm
                INNER JOIN Form ON Form.Id = ValuesAttrToForm.Id_Form
                WHERE ValuesAttrToForm.Id_Attribute = " + idAttr + " AND Form.Name LIKE '" + nameForm + "'";

            return GetResult();
        }

        public string GetComment(int idAttr, int idForm)
        {
            query = @"SELECT AllComments.ExcelComment From AllComments 
                    INNER JOIN AttributeCommentsNEW ON AttributeCommentsNEW.Id_AllComments = AllComments.Id
                    INNER JOIN ValuesAttrToForm ON ValuesAttrToForm.Id = AttributeCommentsNEW.Id_ValuesAttrToForm 
                    WHERE ValuesAttrToForm.Id_Attribute = " + idAttr + " AND ValuesAttrToForm.Id_Form = " + idForm;

            return GetResult();
        }

        public override int GetIdByName(string nameAttr)
        {
            query = "SELECT Id from Attributes WHERE Name LIKE '" + nameAttr + "'";
            return GetResultInt();
        }

        public int GetPosition(string nameAttr)
        {
            query = "SELECT Position From Attributes Where Name LIKE '" + nameAttr + "'";
            return GetResultInt();
        }

        public int GetPosition(int idAttr)
        {
            query = "SELECT Position From Attributes Where Id= " + idAttr;
            return GetResultInt();
        }

        public List<string> GetAllAttributes(bool orderByPosition = true)
        {
            query = "SELECT Name From Attributes";
            if (orderByPosition)
                query += " ORDER BY Position";
            return GetResults();
        }

        public List<string> GetAllFormsAssignedTo(int idAttr)
        {
            query = @"SELECT Form.Name FROM Form 
                    INNER JOIN AttributeToTheForm ON Form.Id = AttributeToTheForm.Id_Form 
                    WHERE Id_Attribute = " + idAttr.ToString();

            return GetResults();
        }

    }

}
