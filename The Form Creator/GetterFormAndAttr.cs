﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace The_Form_Creator
{
    abstract class GetterFormAndAttr
    {
        protected SQLiteEasierCommands sqliteCmd;
        protected string query;
        protected SQLiteDataReader reader;

        public GetterFormAndAttr(SQLiteEasierCommands sqliteCmd)
        {
            this.sqliteCmd = sqliteCmd;
        }

        protected void ExecutedReader()
        {
            reader = sqliteCmd.GetExecutedReader(query);
        }

//        protected List<string> getResults(string query)
//{

//        }

        protected string GetResult()
        {
            ExecutedReader();
            reader.Read();
            string result = (reader.HasRows) ? reader[0].ToString() : "";
            reader.Close();

            return result;
        }

        protected List<string> GetResults()
        {
            List<string> list = new List<string>();
            ExecutedReader();

            while(reader.Read())
            {
                list.Add(reader[0].ToString());
            }
            reader.Close();
       
            return list;
        }

        protected List<int> GetResultsInt()
        {
            List<int> list = new List<int>();
            int numVal;
            List<string> temp = GetResults();

            foreach (var item in temp)
            {
                
                if (!Int32.TryParse(item, out numVal))
                    numVal = -1;
                list.Add(numVal);
            }

            return list;
        }


        protected int GetResultInt()
        {
            int numVal;
            if (!Int32.TryParse(GetResult(), out numVal))
                numVal = -1;

            return numVal;
        }

        public int GetIdAttrToForm(int idAttr, int idForm)
        {
            query = "SELECT Id From AttributeToTheForm WHERE Id_Attribute = " + idAttr + " AND Id_Form = " + idForm;
            return GetResultInt();
        }

        public int GetIdValuesAttrToForm(int idAttr, int idForm)
        {
            query = "SELECT Id From ValuesAttrToForm WHERE Id_Attribute = " + idAttr + " AND Id_Form = " + idForm;
            return GetResultInt();
        }

        abstract public string GetName(int id);
        abstract public int GetIdByName(string nameAttr);
        abstract public List<string> GetFunctions(string name);
        abstract public List<string> GetComments(string name);


        // abstract public string GetFunction(int idAttr, int idForm);
        // abstract public string GetFunction(int idAttr, string nameForm);
        // abstract public string GetComment(int idAttr, string nameForm);
        // abstract public string GetComment(int idAttr, int idForm);


    }
}
