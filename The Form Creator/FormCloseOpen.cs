﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Form_Creator
{
    class FormCloseOpen
    {
        static public void FormClose(Form formToClose)
        {
            //formToClose.Hide();//
            formToClose.Visible = false;
        }

        static public void FormOpen(Form formToOpen, Form thisForm)
        {
            formToOpen.StartPosition = FormStartPosition.Manual;
            formToOpen.Location = thisForm.Location;
            formToOpen.Height = thisForm.Height;
            formToOpen.Width = thisForm.Width;
            formToOpen.Show();
        }

        static public void FormCloseAndOpen(Form formToOpen, Form formToClose)
        {
            FormOpen(formToOpen, formToClose);
            FormClose(formToClose);
        }
    }
}
